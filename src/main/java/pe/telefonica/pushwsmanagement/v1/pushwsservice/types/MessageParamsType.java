
package pe.telefonica.pushwsmanagement.v1.pushwsservice.types;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para messageParamsType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="messageParamsType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="smsCommand" type="{http://telefonica.pe/PushWsManagement/V1/PushWsService/types}smsCommandType" minOccurs="0"/&gt;
 *         &lt;element name="satCommand" type="{http://telefonica.pe/PushWsManagement/V1/PushWsService/types}satCommandType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "messageParamsType", propOrder = {
    "smsCommand",
    "satCommand"
})
public class MessageParamsType {

    protected SmsCommandType smsCommand;
    protected List<SatCommandType> satCommand;
    @XmlAttribute(name = "type")
    protected String type;

    /**
     * Obtiene el valor de la propiedad smsCommand.
     * 
     * @return
     *     possible object is
     *     {@link SmsCommandType }
     *     
     */
    public SmsCommandType getSmsCommand() {
        return smsCommand;
    }

    /**
     * Define el valor de la propiedad smsCommand.
     * 
     * @param value
     *     allowed object is
     *     {@link SmsCommandType }
     *     
     */
    public void setSmsCommand(SmsCommandType value) {
        this.smsCommand = value;
    }

    /**
     * Gets the value of the satCommand property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the satCommand property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSatCommand().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SatCommandType }
     * 
     * 
     */
    public List<SatCommandType> getSatCommand() {
        if (satCommand == null) {
            satCommand = new ArrayList<SatCommandType>();
        }
        return this.satCommand;
    }

    /**
     * Obtiene el valor de la propiedad type.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getType() {
        return type;
    }

    /**
     * Define el valor de la propiedad type.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setType(String value) {
        this.type = value;
    }

}
