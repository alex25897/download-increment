
package pe.telefonica.pushwsmanagement.v1.pushwsservice.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import pe.telefonica.tefrequestheader.v1.TefHeaderReq;


/**
 * <p>Clase Java para SendPromotionalMessageRequestType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="SendPromotionalMessageRequestType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://telefonica.pe/TefRequestHeader/V1}TefHeaderReq"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SendPromotionalMessageRequestData" type="{http://telefonica.pe/PushWsManagement/V1/PushWsService/types}SendPromotionalMessageRequestDataType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SendPromotionalMessageRequestType", propOrder = {
    "sendPromotionalMessageRequestData"
})
public class SendPromotionalMessageRequestType
    extends TefHeaderReq
{

    @XmlElement(name = "SendPromotionalMessageRequestData", required = true)
    protected SendPromotionalMessageRequestDataType sendPromotionalMessageRequestData;

    /**
     * Obtiene el valor de la propiedad sendPromotionalMessageRequestData.
     * 
     * @return
     *     possible object is
     *     {@link SendPromotionalMessageRequestDataType }
     *     
     */
    public SendPromotionalMessageRequestDataType getSendPromotionalMessageRequestData() {
        return sendPromotionalMessageRequestData;
    }

    /**
     * Define el valor de la propiedad sendPromotionalMessageRequestData.
     * 
     * @param value
     *     allowed object is
     *     {@link SendPromotionalMessageRequestDataType }
     *     
     */
    public void setSendPromotionalMessageRequestData(SendPromotionalMessageRequestDataType value) {
        this.sendPromotionalMessageRequestData = value;
    }

}
