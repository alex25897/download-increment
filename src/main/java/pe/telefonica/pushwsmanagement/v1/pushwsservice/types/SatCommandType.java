
package pe.telefonica.pushwsmanagement.v1.pushwsservice.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para satCommandType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="satCommandType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="textString" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="240"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="offeringCode" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="150"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="menuTitle" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="40"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="menuItemList" type="{http://telefonica.pe/PushWsManagement/V1/PushWsService/types}menuItemListType" minOccurs="0"/&gt;
 *         &lt;element name="ussdString" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="32"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="smsDestination" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="24"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="smsText" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="160"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="callDestination" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="40"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="url" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="256"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="seqId" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "satCommandType", propOrder = {
    "textString",
    "offeringCode",
    "menuTitle",
    "menuItemList",
    "ussdString",
    "smsDestination",
    "smsText",
    "callDestination",
    "url"
})
public class SatCommandType {

    protected String textString;
    protected String offeringCode;
    protected String menuTitle;
    protected MenuItemListType menuItemList;
    protected String ussdString;
    protected String smsDestination;
    protected String smsText;
    protected String callDestination;
    protected String url;
    @XmlAttribute(name = "type")
    protected String type;
    @XmlAttribute(name = "seqId")
    protected Integer seqId;

    /**
     * Obtiene el valor de la propiedad textString.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTextString() {
        return textString;
    }

    /**
     * Define el valor de la propiedad textString.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTextString(String value) {
        this.textString = value;
    }

    /**
     * Obtiene el valor de la propiedad offeringCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOfferingCode() {
        return offeringCode;
    }

    /**
     * Define el valor de la propiedad offeringCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOfferingCode(String value) {
        this.offeringCode = value;
    }

    /**
     * Obtiene el valor de la propiedad menuTitle.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMenuTitle() {
        return menuTitle;
    }

    /**
     * Define el valor de la propiedad menuTitle.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMenuTitle(String value) {
        this.menuTitle = value;
    }

    /**
     * Obtiene el valor de la propiedad menuItemList.
     * 
     * @return
     *     possible object is
     *     {@link MenuItemListType }
     *     
     */
    public MenuItemListType getMenuItemList() {
        return menuItemList;
    }

    /**
     * Define el valor de la propiedad menuItemList.
     * 
     * @param value
     *     allowed object is
     *     {@link MenuItemListType }
     *     
     */
    public void setMenuItemList(MenuItemListType value) {
        this.menuItemList = value;
    }

    /**
     * Obtiene el valor de la propiedad ussdString.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUssdString() {
        return ussdString;
    }

    /**
     * Define el valor de la propiedad ussdString.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUssdString(String value) {
        this.ussdString = value;
    }

    /**
     * Obtiene el valor de la propiedad smsDestination.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSmsDestination() {
        return smsDestination;
    }

    /**
     * Define el valor de la propiedad smsDestination.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSmsDestination(String value) {
        this.smsDestination = value;
    }

    /**
     * Obtiene el valor de la propiedad smsText.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSmsText() {
        return smsText;
    }

    /**
     * Define el valor de la propiedad smsText.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSmsText(String value) {
        this.smsText = value;
    }

    /**
     * Obtiene el valor de la propiedad callDestination.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCallDestination() {
        return callDestination;
    }

    /**
     * Define el valor de la propiedad callDestination.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCallDestination(String value) {
        this.callDestination = value;
    }

    /**
     * Obtiene el valor de la propiedad url.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUrl() {
        return url;
    }

    /**
     * Define el valor de la propiedad url.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUrl(String value) {
        this.url = value;
    }

    /**
     * Obtiene el valor de la propiedad type.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getType() {
        return type;
    }

    /**
     * Define el valor de la propiedad type.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setType(String value) {
        this.type = value;
    }

    /**
     * Obtiene el valor de la propiedad seqId.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSeqId() {
        return seqId;
    }

    /**
     * Define el valor de la propiedad seqId.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSeqId(Integer value) {
        this.seqId = value;
    }

}
