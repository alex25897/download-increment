
package pe.telefonica.pushwsmanagement.v1.pushwsservice.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import pe.telefonica.tefresponseheader.v1.TefHeaderRes;


/**
 * <p>Clase Java para SendPromotionalMessageResponseType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="SendPromotionalMessageResponseType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://telefonica.pe/TefResponseHeader/V1}TefHeaderRes"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SendPromotionalMessageResponseData" type="{http://telefonica.pe/PushWsManagement/V1/PushWsService/types}SendPromotionalMessageResponseDataType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SendPromotionalMessageResponseType", propOrder = {
    "sendPromotionalMessageResponseData"
})
public class SendPromotionalMessageResponseType
    extends TefHeaderRes
{

    @XmlElement(name = "SendPromotionalMessageResponseData")
    protected SendPromotionalMessageResponseDataType sendPromotionalMessageResponseData;

    /**
     * Obtiene el valor de la propiedad sendPromotionalMessageResponseData.
     * 
     * @return
     *     possible object is
     *     {@link SendPromotionalMessageResponseDataType }
     *     
     */
    public SendPromotionalMessageResponseDataType getSendPromotionalMessageResponseData() {
        return sendPromotionalMessageResponseData;
    }

    /**
     * Define el valor de la propiedad sendPromotionalMessageResponseData.
     * 
     * @param value
     *     allowed object is
     *     {@link SendPromotionalMessageResponseDataType }
     *     
     */
    public void setSendPromotionalMessageResponseData(SendPromotionalMessageResponseDataType value) {
        this.sendPromotionalMessageResponseData = value;
    }

}
