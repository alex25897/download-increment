
package pe.telefonica.pushwsmanagement.v1.pushwsservice.types;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the pe.telefonica.pushwsmanagement.v1.pushwsservice.types package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _SendPromotionalMessageRequest_QNAME = new QName("http://telefonica.pe/PushWsManagement/V1/PushWsService/types", "SendPromotionalMessageRequest");
    private final static QName _SendPromotionalMessageResponse_QNAME = new QName("http://telefonica.pe/PushWsManagement/V1/PushWsService/types", "SendPromotionalMessageResponse");
    private final static QName _SendPromotionalMessageFault_QNAME = new QName("http://telefonica.pe/PushWsManagement/V1/PushWsService/types", "SendPromotionalMessageFault");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: pe.telefonica.pushwsmanagement.v1.pushwsservice.types
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link SendPromotionalMessageRequestType }
     * 
     */
    public SendPromotionalMessageRequestType createSendPromotionalMessageRequestType() {
        return new SendPromotionalMessageRequestType();
    }

    /**
     * Create an instance of {@link SendPromotionalMessageResponseType }
     * 
     */
    public SendPromotionalMessageResponseType createSendPromotionalMessageResponseType() {
        return new SendPromotionalMessageResponseType();
    }

    /**
     * Create an instance of {@link SendPromotionalMessageFaultType }
     * 
     */
    public SendPromotionalMessageFaultType createSendPromotionalMessageFaultType() {
        return new SendPromotionalMessageFaultType();
    }

    /**
     * Create an instance of {@link SendPromotionalMessageRequestDataType }
     * 
     */
    public SendPromotionalMessageRequestDataType createSendPromotionalMessageRequestDataType() {
        return new SendPromotionalMessageRequestDataType();
    }

    /**
     * Create an instance of {@link SendPromotionalMessageResponseDataType }
     * 
     */
    public SendPromotionalMessageResponseDataType createSendPromotionalMessageResponseDataType() {
        return new SendPromotionalMessageResponseDataType();
    }

    /**
     * Create an instance of {@link MessageParamsType }
     * 
     */
    public MessageParamsType createMessageParamsType() {
        return new MessageParamsType();
    }

    /**
     * Create an instance of {@link SmsCommandType }
     * 
     */
    public SmsCommandType createSmsCommandType() {
        return new SmsCommandType();
    }

    /**
     * Create an instance of {@link SatCommandType }
     * 
     */
    public SatCommandType createSatCommandType() {
        return new SatCommandType();
    }

    /**
     * Create an instance of {@link MenuItemListType }
     * 
     */
    public MenuItemListType createMenuItemListType() {
        return new MenuItemListType();
    }

    /**
     * Create an instance of {@link MenuItemType }
     * 
     */
    public MenuItemType createMenuItemType() {
        return new MenuItemType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendPromotionalMessageRequestType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SendPromotionalMessageRequestType }{@code >}
     */
    @XmlElementDecl(namespace = "http://telefonica.pe/PushWsManagement/V1/PushWsService/types", name = "SendPromotionalMessageRequest")
    public JAXBElement<SendPromotionalMessageRequestType> createSendPromotionalMessageRequest(SendPromotionalMessageRequestType value) {
        return new JAXBElement<SendPromotionalMessageRequestType>(_SendPromotionalMessageRequest_QNAME, SendPromotionalMessageRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendPromotionalMessageResponseType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SendPromotionalMessageResponseType }{@code >}
     */
    @XmlElementDecl(namespace = "http://telefonica.pe/PushWsManagement/V1/PushWsService/types", name = "SendPromotionalMessageResponse")
    public JAXBElement<SendPromotionalMessageResponseType> createSendPromotionalMessageResponse(SendPromotionalMessageResponseType value) {
        return new JAXBElement<SendPromotionalMessageResponseType>(_SendPromotionalMessageResponse_QNAME, SendPromotionalMessageResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendPromotionalMessageFaultType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SendPromotionalMessageFaultType }{@code >}
     */
    @XmlElementDecl(namespace = "http://telefonica.pe/PushWsManagement/V1/PushWsService/types", name = "SendPromotionalMessageFault")
    public JAXBElement<SendPromotionalMessageFaultType> createSendPromotionalMessageFault(SendPromotionalMessageFaultType value) {
        return new JAXBElement<SendPromotionalMessageFaultType>(_SendPromotionalMessageFault_QNAME, SendPromotionalMessageFaultType.class, null, value);
    }

}
