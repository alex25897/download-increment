
package pe.telefonica.tefrequestheader.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import pe.telefonica.pushwsmanagement.v1.pushwsservice.types.SendPromotionalMessageRequestType;


/**
 * <p>Clase Java para TefHeaderReq complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="TefHeaderReq"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="TefHeaderReq" type="{http://telefonica.pe/TefRequestHeader/V1}TefHeaderReqType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TefHeaderReq", propOrder = {
    "tefHeaderReq"
})
@XmlSeeAlso({
    SendPromotionalMessageRequestType.class
})
public class TefHeaderReq {

    @XmlElement(name = "TefHeaderReq", required = true)
    protected TefHeaderReqType tefHeaderReq;

    /**
     * Obtiene el valor de la propiedad tefHeaderReq.
     * 
     * @return
     *     possible object is
     *     {@link TefHeaderReqType }
     *     
     */
    public TefHeaderReqType getTefHeaderReq() {
        return tefHeaderReq;
    }

    /**
     * Define el valor de la propiedad tefHeaderReq.
     * 
     * @param value
     *     allowed object is
     *     {@link TefHeaderReqType }
     *     
     */
    public void setTefHeaderReq(TefHeaderReqType value) {
        this.tefHeaderReq = value;
    }

}
