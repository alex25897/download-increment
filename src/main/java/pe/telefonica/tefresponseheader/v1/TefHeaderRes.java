
package pe.telefonica.tefresponseheader.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import pe.telefonica.pushwsmanagement.v1.pushwsservice.types.SendPromotionalMessageResponseType;


/**
 * <p>Clase Java para TefHeaderRes complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="TefHeaderRes"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="TefHeaderRes" type="{http://telefonica.pe/TefResponseHeader/V1}TefHeaderResType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TefHeaderRes", propOrder = {
    "tefHeaderRes"
})
@XmlSeeAlso({
    SendPromotionalMessageResponseType.class
})
public class TefHeaderRes {

    @XmlElement(name = "TefHeaderRes", required = true)
    protected TefHeaderResType tefHeaderRes;

    /**
     * Obtiene el valor de la propiedad tefHeaderRes.
     * 
     * @return
     *     possible object is
     *     {@link TefHeaderResType }
     *     
     */
    public TefHeaderResType getTefHeaderRes() {
        return tefHeaderRes;
    }

    /**
     * Define el valor de la propiedad tefHeaderRes.
     * 
     * @param value
     *     allowed object is
     *     {@link TefHeaderResType }
     *     
     */
    public void setTefHeaderRes(TefHeaderResType value) {
        this.tefHeaderRes = value;
    }

}
