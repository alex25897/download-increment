package com.telefonica.viewscheduled.pojo;

import java.util.Map;

import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class NotifyAllSubsReq {

    @NotNull(message = "El campo es requerido")
    private String		mediaType;
    @NotNull(message = "El campo es requerido")
    private String		templateId;
    @NotNull(message = "El campo es requerido")
    private Map<String, String>	templateParam;
    private EmailData		email;
    private SmsData		sms;

}
