package com.telefonica.viewscheduled.pojo;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class EmailData {

    private List<String> email;
    private List<String> emailCc;
    private List<String> emailBc;

    public List<String> getEmail() {
	if (email == null) {
	    return new ArrayList<>();
	}
	return email;
    }

    public List<String> getEmailCc() {
	if (emailCc == null) {
	    return new ArrayList<>();
	}
	return emailCc;
    }

    public List<String> getEmailBc() {
	if (emailBc == null) {
	    return new ArrayList<>();
	}
	return emailBc;
    }
}
