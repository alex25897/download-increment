package com.telefonica.viewscheduled.pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class RespObject {

    private String  tracking;
    private String  transactionWs;
    private String  operationCode;
    private String  reason;
    private String  detail;
    private Boolean result;

}