package com.telefonica.viewscheduled.pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class ReqObject {
    private String transactionID;

    /** for send SMS **/
    private String phoneNumber;
    private String message;


}
