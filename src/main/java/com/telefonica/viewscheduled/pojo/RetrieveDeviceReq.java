package com.telefonica.viewscheduled.pojo;

import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class RetrieveDeviceReq {
    @NotNull(message = "Este campo es requerido")
    private String countryCode;
    @NotNull(message = "Este campo es requerido")
    private String number;
}
