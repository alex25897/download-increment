package com.telefonica.viewscheduled.pojo;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class DataReq {

    @Valid
    @NotNull(message = "El campo es requerido")
    private HeaderReq		header;
    @NotNull(message = "El campo es requerido")
    private String		id;
    @Valid
    private RetrieveDeviceReq	retrieveDevice;
    @Valid
    private ChangeProdStatusReq	changeProdStatus;
    @Valid
    private NotifyAllSubsReq	notifyAllSubsReq;

}
