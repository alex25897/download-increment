package com.telefonica.viewscheduled.pojo;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class SmsData {

    private List<String> phoneNumber;

    public List<String> getPhoneNumber() {
	if (phoneNumber == null) {
	    return new ArrayList<>();
	}
	return phoneNumber;
    }

}
