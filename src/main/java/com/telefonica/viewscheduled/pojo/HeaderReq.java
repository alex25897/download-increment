package com.telefonica.viewscheduled.pojo;

import lombok.Data;

@Data
public class HeaderReq {
    private String userLogin;
    private String serviceChannel;
    private String sessionCode;
    private String application;
    private String idMessage;
    private String ipAddress;
    private String functionalityCode;
    private String transactionTimestamp;
    private String serviceName;
    private String version;
}
