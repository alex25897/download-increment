package com.telefonica.viewscheduled.pojo;

import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class ChangeProdStatusReq {
    private String instanceId;
    private String externalProductID;

    @NotNull(message = "Este campo es requerido")
    private String orderAction;

    private String reasonCode;
    private String activityReason;

    private String reporterFirstName;
    private String reporterLastName;
    private String identificationType;
    private String identificationNumber;
    private String reporterNumber;

}
