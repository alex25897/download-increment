package com.telefonica.viewscheduled.proxy;

import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.telefonica.viewscheduled.commons.Constant;
import com.telefonica.viewscheduled.commons.CustomProp;
import com.telefonica.viewscheduled.generate.ProductOrderUpdateRequestType;

@Service
public class SolicitationTheft {

    private static final Logger LOGGER = LogManager.getLogger(SolicitationTheft.class);

    @Autowired
    private RestTemplate restemplate;
    @Autowired
    private CustomProp	 prop;

    public void updateTheftcut(String customerId, String orderId, ProductOrderUpdateRequestType requestProductOrder) {
	HttpHeaders header = new HttpHeaders();
	header.set(Constant.SUBSCRIPTION_KEY, Constant.SUBSCRIPTION_KEY_VAL);
	header.set(Constant.UNI_USER, Constant.UNI_USER_VAL);
	header.set(Constant.CONTENT_TYPE, Constant.CONTENT_TYPE_VAL);
	header.set(Constant.UNI_SERVICEID, Constant.UNI_SERVICEID_VAL);
	header.set(Constant.UNI_PID, Constant.UNI_PID_VAL);

	Map<String, String> params = new HashMap<>();
	params.put("customerId", customerId);
	params.put("orderId", orderId);

	UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(prop.getUrlsolicitation());

	HttpEntity<ProductOrderUpdateRequestType> request = new HttpEntity<>(requestProductOrder, header);
	LOGGER.info("Uri: " + builder.buildAndExpand(params).toUri());
	try {
	    restemplate.exchange(builder.buildAndExpand(params).toUri(), HttpMethod.PUT, request, Object.class);
	} catch (HttpClientErrorException e) {
	    LOGGER.error("Error when consume rest service Solicitation: " + e.getMessage());
	    LOGGER.error("Body Error: " + e.getResponseBodyAsString());
	} catch (Exception e) {
	    LOGGER.error("Error when consume rest service Solicitation: " + e.getMessage());
	    LOGGER.error("Error not contemplated: " + e);
	}

    }
}
