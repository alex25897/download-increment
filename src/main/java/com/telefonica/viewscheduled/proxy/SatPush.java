package com.telefonica.viewscheduled.proxy;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.namespace.QName;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.ws.client.core.WebServiceTemplate;

import com.telefonica.viewscheduled.commons.Util;
import com.telefonica.viewscheduled.entity.SatPushRequest;

import pe.telefonica.pushwsmanagement.v1.pushwsservice.types.MessageParamsType;
import pe.telefonica.pushwsmanagement.v1.pushwsservice.types.SatCommandType;
import pe.telefonica.pushwsmanagement.v1.pushwsservice.types.SendPromotionalMessageRequestDataType;
import pe.telefonica.pushwsmanagement.v1.pushwsservice.types.SendPromotionalMessageRequestType;
import pe.telefonica.pushwsmanagement.v1.pushwsservice.types.SendPromotionalMessageResponseType;
import pe.telefonica.tefrequestheader.v1.TefHeaderReqType;

@Service
public class SatPush {

    @Autowired
    private WebServiceTemplate wssatpush;
    
    @Async
    public SendPromotionalMessageResponseType sendSatPush (SatPushRequest reqData) throws DatatypeConfigurationException {
	
	SendPromotionalMessageRequestType request = new SendPromotionalMessageRequestType();
	
	TefHeaderReqType header = new TefHeaderReqType();
	/** settear header **/
	header.setUserLogin("USER_SAT");
	header.setServiceChannel("CEC");
	header.setApplication("OMS");
	header.setIpAddress("10.40.71.12");
	header.setTransactionTimestamp(Util.getXMLGCalendar());
	header.setServiceName("SendPromotionalMessage");
	header.setVersion("v1.0");
	request.setTefHeaderReq(header);
	
	SendPromotionalMessageRequestDataType data = new SendPromotionalMessageRequestDataType();
	data.setCampaignId(reqData.getCampaignId());
	data.setSubscriberId(reqData.getSubscriberId());
	data.setMessageSender(reqData.getMessageSender());
	data.setMessageDestination(reqData.getMessageDestination());
	data.setTransactionId(reqData.getResponseTrackingCd());
	
	MessageParamsType msparams = new MessageParamsType();
	msparams.setType("Type8");
	List<SatCommandType> lstSatcommand = new ArrayList<>();
	SatCommandType satcommand = new SatCommandType();
	satcommand.setType("DisplayText");
	satcommand.setSeqId(1);
	satcommand.setTextString(reqData.getMessageText());
	satcommand.setOfferingCode("DOWN_INC");
	lstSatcommand.add(satcommand);	
	SatCommandType satcommand1 = new SatCommandType();
	satcommand1.setType("LaunchBrowser");
	satcommand1.setSeqId(2);
	satcommand1.setTextString("App Mi Movistar");
	satcommand1.setUrl("https://movistar.onelink.me/7Cca/5fc1c625");
	lstSatcommand.add(satcommand1);
	msparams.getSatCommand().addAll(lstSatcommand);
	data.setMessageParams(msparams);
	request.setSendPromotionalMessageRequestData(data);

//	SendPromotionalMessageRequestDataType data = new SendPromotionalMessageRequestDataType();
//	data.setCampaignId(reqData.getCampaignId());
//	data.setSubscriberId(reqData.getSubscriberId());
//	data.setMessageSender(reqData.getMessageSender());
//	data.setMessageDestination(reqData.getMessageDestination());
//	data.setTransactionId(reqData.getResponseTrackingCd());
//	
//	MessageParamsType msparams = new MessageParamsType();
//	msparams.setType("Type1");
//	List<SatCommandType> lstSatcommand = new ArrayList<>();
//	SatCommandType satcommand = new SatCommandType();
//	satcommand.setType("DisplayText");
//	satcommand.setSeqId(1);
//	satcommand.setTextString(reqData.getMessageText());
//	satcommand.setOfferingCode("CODE_FRACC");
//	lstSatcommand.add(satcommand);
//	msparams.getSatCommand().addAll(lstSatcommand);
//	data.setMessageParams(msparams);
//	data.setMessageType("Type1");
//	request.setSendPromotionalMessageRequestData(data);
	
	@SuppressWarnings("unchecked")
	JAXBElement<SendPromotionalMessageResponseType> response = (JAXBElement<SendPromotionalMessageResponseType>) wssatpush
		.marshalSendAndReceive(new JAXBElement<SendPromotionalMessageRequestType>(
			new QName("http://telefonica.pe/PushWsManagement/V1/PushWsService/types",
				"SendPromotionalMessageRequest"),
			SendPromotionalMessageRequestType.class, request));
	
	return response.getValue();
    }
}
