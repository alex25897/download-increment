package com.telefonica.viewscheduled.proxy;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.telefonica.viewscheduled.commons.CustomProp;
import com.telefonica.viewscheduled.pojo.ReqObject;
import com.telefonica.viewscheduled.pojo.RespObject;

/**
 * 
 * @Author: jomapozo.
 * @Datecreation: 20 set. 2018 10:27:38
 * @FileName: UtilRestClient.java
 * @AuthorCompany: Telefonica
 * @version: 0.1
 * @Description: Class for Rest Proxy clients
 */
@Service
public class SendMessage {

    private static final Logger LOGGER = LogManager.getLogger(SendMessage.class);

    @Autowired
    private CustomProp	 customProp;
    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    private ObjectMapper objMapper;

    public RespObject sendMessage(ReqObject request, String url) throws JsonProcessingException {
	RespObject resp = new RespObject();
	HttpEntity<ReqObject> requestEntity = new HttpEntity<>(request);
	String json = objMapper.writeValueAsString(request);
	LOGGER.info("#Start Method [sendMessage] -> {}", json);
	LOGGER.info("[Url] -> {}", url);
	try {
	    ResponseEntity<RespObject> response = restTemplate.exchange(customProp.getIntegrationhost() + url, HttpMethod.POST,
		    requestEntity, RespObject.class);
	    resp = response.getBody();
	    return resp;
	} catch (HttpClientErrorException e) {
	    LOGGER.error("Error when consume rest service SendMenssage: " + e.getMessage());
	    LOGGER.error("Body Error: " + e.getResponseBodyAsString());
	    resp.setReason(e.getResponseBodyAsString());
	    resp.setResult(Boolean.FALSE);
	    return resp;
	} catch (Exception e) {
	    LOGGER.error("Error when consume rest service SendMenssage: " + e.getMessage());
	    LOGGER.error("Error not contemplated: " + e);
	    resp.setResult(Boolean.FALSE);
	    return resp;
	} finally {
	    String jsonResp = objMapper.writeValueAsString(resp);
	    LOGGER.info("#End Method [sendMessage] -> {}", jsonResp);
	}
    }

}
