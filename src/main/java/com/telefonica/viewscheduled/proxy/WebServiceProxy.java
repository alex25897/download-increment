package com.telefonica.viewscheduled.proxy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.Getter;

@Service
@Getter
public class WebServiceProxy {

    @Autowired
    private SendMessage	      sendMessage;
    @Autowired
    private SolicitationTheft solicitation;

}
