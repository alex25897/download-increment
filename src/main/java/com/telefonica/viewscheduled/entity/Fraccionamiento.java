package com.telefonica.viewscheduled.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

@Entity
@Table(name = "FRACCIONAMIENTO_WA_V2")
@Data
public class Fraccionamiento implements Serializable{

    private static final long serialVersionUID = -2483134721913588930L;
    @Id
    @Column(name = "ABONADO_CD")
    private String  abonadocd;
    @Column(name = "TELEFONO")
    private String  telefono;
    @Column(name = "MENSAJE")
    private String mensaje;

}
