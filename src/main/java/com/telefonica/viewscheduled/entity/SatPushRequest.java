package com.telefonica.viewscheduled.entity;

import lombok.Data;

@Data
public class SatPushRequest {

    private String type	= "1";
    private String responseTrackingCd="1256110301";
    private String campaignId="SAT_FRAC_D";
    private String subscriberId="123456789";
    private String messageSender="1515";
    private String messageDestination="996945998";
    private String messageText="prueba";
    private String offerId;

}
