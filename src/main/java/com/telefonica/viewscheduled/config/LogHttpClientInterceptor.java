package com.telefonica.viewscheduled.config;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.ws.client.WebServiceClientException;
import org.springframework.ws.client.support.interceptor.ClientInterceptor;
import org.springframework.ws.context.MessageContext;

public class LogHttpClientInterceptor implements ClientInterceptor {

    private static final Logger LOGGER = LogManager.getLogger(LogHttpClientInterceptor.class);

    @Override
    public boolean handleRequest(MessageContext messageContext) throws WebServiceClientException {
	return true;
    }

    @Override
    public boolean handleResponse(MessageContext messageContext) throws WebServiceClientException {
	return true;
    }

    @Override
    public boolean handleFault(MessageContext messageContext) throws WebServiceClientException {
	try (ByteArrayOutputStream baosReq = new ByteArrayOutputStream(); ByteArrayOutputStream baosResp = new ByteArrayOutputStream();) {
	    messageContext.getResponse().writeTo(baosResp);
	    String response = baosResp.toString(StandardCharsets.UTF_8.name());
	    LOGGER.trace("[Fault]: " + response);
	} catch (IOException e) {
	    LOGGER.error("Error obteniendo request y response soap: " + e);
	}
	return true;
    }

    @Override
    public void afterCompletion(MessageContext messageContext, Exception ex) throws WebServiceClientException {

	try (ByteArrayOutputStream baosReq = new ByteArrayOutputStream(); ByteArrayOutputStream baosResp = new ByteArrayOutputStream();) {
	    messageContext.getRequest().writeTo(baosReq);
	    String request = baosReq.toString(StandardCharsets.UTF_8.name());
	    LOGGER.info("[Request]: " + request);

	    messageContext.getResponse().writeTo(baosResp);
	    String response = baosResp.toString(StandardCharsets.UTF_8.name());
	    LOGGER.info("[Response]: " + response);
	} catch (IOException e) {
	    LOGGER.error("Error obteniendo request y response soap: " + e);
	}

    }

}
