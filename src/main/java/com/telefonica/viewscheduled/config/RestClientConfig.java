package com.telefonica.viewscheduled.config;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import com.telefonica.viewscheduled.commons.CustomProp;

@Configuration
public class RestClientConfig {

    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder, CustomProp custom) {
	builder.setConnectTimeout(custom.getConnectiontimeout());
	builder.setReadTimeout(custom.getReadtimeout());
	return builder.build();
    }
}
