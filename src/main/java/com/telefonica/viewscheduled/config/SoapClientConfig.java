package com.telefonica.viewscheduled.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.client.support.interceptor.ClientInterceptor;
import org.springframework.ws.transport.WebServiceMessageSender;
import org.springframework.ws.transport.http.HttpComponentsMessageSender;

import com.telefonica.viewscheduled.commons.CustomProp;

@Configuration
public class SoapClientConfig {

    @Autowired
    private CustomProp prop;

    @Bean
    public Jaxb2Marshaller marshaller() {
	Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
	//marshaller.setContextPaths("pe.telefonica.pushwsmanagement.v1", "pe.telefonica.teferror.v1", "pe.telefonica.tefrequestheader.v1",
	//	"pe.telefonica.tefresponseheader.v1", "pe.telefonica.pushwsmanagement.v1.pushwsservice.types");
	marshaller.setPackagesToScan("pe.telefonica.pushwsmanagement.v1", "pe.telefonica.teferror.v1", "pe.telefonica.tefrequestheader.v1",
		"pe.telefonica.tefresponseheader.v1", "pe.telefonica.pushwsmanagement.v1.pushwsservice.types");
	
	marshaller.setCheckForXmlRootElement(Boolean.TRUE);
	return marshaller;
    }

    @Bean
    public WebServiceMessageSender wsMessageSender() {
	HttpComponentsMessageSender sender = new HttpComponentsMessageSender();
	sender.setConnectionTimeout(prop.getConnectiontimeout());
	sender.setReadTimeout(prop.getReadtimeout());
	return sender;
    }
    
    
    @Bean(name = "wssatpush")
    public WebServiceTemplate wsSatpush() {
	WebServiceTemplate webServiceTemplate = new WebServiceTemplate();
	webServiceTemplate.setMarshaller(marshaller());
	webServiceTemplate.setUnmarshaller(marshaller());
	webServiceTemplate.setDefaultUri(prop.getUrlsatpush());
	webServiceTemplate.setMessageSender(wsMessageSender());
	return webServiceTemplate;
    }
    
}
