package com.telefonica.viewscheduled.scheduled;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.telefonica.viewscheduled.entity.Fraccionamiento;
import com.telefonica.viewscheduled.entity.SatPushRequest;
import com.telefonica.viewscheduled.proxy.SatPush;
import com.telefonica.viewscheduled.repository.ViewfinderRepo;

@Service
public class QueryViewfinder {

    private static final Logger LOGGER = LogManager.getLogger(QueryViewfinder.class);

    @Autowired
    private ViewfinderRepo viewRepo;
    @Autowired
    private ObjectMapper   mapper;
    @Autowired
    private SatPush	   satpush;

    /**
     * 
     * @throws InterruptedException
     **/
    @Scheduled(cron = "0 23 14 31 05 ?")
    public void executeWaFracc() throws InterruptedException {
	LOGGER.info("Init Scheduled - [executeWaFracc]");
	int batchSize = 50;
	int init = 0;
	List<Fraccionamiento> lstFrac = (List<Fraccionamiento>) viewRepo.findAll();

	for (Fraccionamiento fracc : lstFrac) {
	    LOGGER.info("[abonado]" + fracc.getAbonadocd());
	    /** create object **/
	    SatPushRequest data = new SatPushRequest();
	    data.setType("1");
	        data.setCampaignId("SATPUSH_FRAC_D");
	        data.setMessageSender("1515");
	        data.setResponseTrackingCd("1234");
	        data.setOfferId("CODE_FRACC");
	        data.setSubscriberId("123456789");
	        data.setMessageText("prueba");
	        data.setMessageDestination("996945998");

	    try {
		 satpush.sendSatPush(data);
	    } catch (Exception e) {
		LOGGER.error("[ERROR WS SATPUSH PARA ABONADO] " + fracc.getAbonadocd());
	    }
	    
	    if (init == batchSize) {
		Thread.sleep(4000);
		init = 0;
	    }
	    init++;
	}
    }

}
