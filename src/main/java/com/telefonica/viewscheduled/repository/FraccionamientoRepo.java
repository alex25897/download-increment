package com.telefonica.viewscheduled.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.telefonica.viewscheduled.entity.Fraccionamiento;

@Repository
public interface FraccionamientoRepo extends CrudRepository<Fraccionamiento, Serializable>{

    
}
