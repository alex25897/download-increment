package com.telefonica.viewscheduled.commons;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Service;

import lombok.Data;

@Service
@Data
@ConfigurationProperties(prefix = "viewfinder.ws")
public class CustomProp {
    private String  urldevicemanagement;
    private String  urlunotification;
    private String  urlorderservices;
    private Integer connectiontimeout;
    private Integer readtimeout;
    private String  urlsms;
    private String  urlemail;
    private String  integrationhost;
    private String  smsmessage;
    private String  urlsolicitation;
    
    //fraccionamiento
    private String  urlsatpush;
}
