package com.telefonica.viewscheduled.commons;

public enum MessageType {
    SMS, EMAIL, NODESEA;
}
