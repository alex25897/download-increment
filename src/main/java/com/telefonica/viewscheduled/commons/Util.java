package com.telefonica.viewscheduled.commons;

import java.util.Date;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

public class Util {

    public static GregorianCalendar getGCalendar() {
	GregorianCalendar gCalendar = new GregorianCalendar();
	gCalendar.setTime(new Date());
	return gCalendar;
    }
    
    public static XMLGregorianCalendar getXMLGCalendar() throws DatatypeConfigurationException {
   	GregorianCalendar gCalendar = new GregorianCalendar();
   	gCalendar.setTime(new Date());
   	return DatatypeFactory.newInstance().newXMLGregorianCalendar(gCalendar);

       }
    

    public static void createTracking(String id) {
	if (id != null) {
	    Thread.currentThread().setName(id);
	}
    }

    private Util() {
    }
}
