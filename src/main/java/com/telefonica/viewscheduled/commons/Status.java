package com.telefonica.viewscheduled.commons;

import lombok.Getter;

@Getter
public enum Status {

    PENDING_1("pending-1"), PENDING_2("pending-2"), PENDING_3("pending-3"), COMPLETED("completed"), CANCELED("canceled"), DEFAULT("");

    String code;

    private Status(String code) {
	this.code = code;
    }
    
    public static Status getStatus(String code) {
	for (Status status : values()) {
	    if (status.getCode().equals(code)) {
		return status;
	    }
	}
	return DEFAULT;
    }
}
