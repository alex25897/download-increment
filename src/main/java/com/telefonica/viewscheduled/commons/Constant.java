package com.telefonica.viewscheduled.commons;

public class Constant {

    /** Header params for retrieve devices detail **/
    public static final String RETRIEVEDEVICE_SERVICENAME    = "DeviceManagement.retrieveDeviceDetails";
    public static final String RETRIEVEDEVICE_VERSION	     = "1.0";
    public static final String RETRIEVEDEVICE_USERLOGIN	     = "mriverase";
    public static final String RETRIEVEDEVICE_SERVICECHANNEL = "CC";
    public static final String RETRIEVEDEVICE_APPLICATION    = "DAAS";

    /** Header params for change product status **/
    public static final String CHANGEPRODUCT_SERVICENAME    = "1";
    public static final String CHANGEPRODUCT_VERSION	    = "1";
    public static final String CHANGEPRODUCT_USERLOGIN	    = "Voice";
    public static final String CHANGEPRODUCT_SERVICECHANNEL = "DSA";
    public static final String CHANGEPRODUCT_APPLICATION    = "DAAS";

    /** Message SMS or Email params **/
    public static final String EMPTY		= "";
    public static final String REQUEST_NAMEMAP	= "NOMBRE_SOLICITANTE";
    public static final String REQUEST_NAME	= "REQUEST_NAME";
    public static final String PHONE		= "PHONE";
    public static final String UNIQUE_CODE	= "UNIQUE_CODE";
    public static final String SMSTEMPLATE_ID	= "VI0001";
    public static final String EMAILTEMPLATE_ID	= "187080";
    public static final String UNIQUE_CODE_SPAN	= "CODIGO_UNICO";
    public static final String HIDDEN_IMEI	= "IMEI_OCULTO";

    /** header solicitation theft **/
    public static final String SUBSCRIPTION_KEY	= "Ocp-Apim-Subscription-Key";
    public static final String UNI_USER		= "UNICA-User";
    public static final String CONTENT_TYPE	= "Content-Type";
    public static final String UNI_SERVICEID	= "UNICA-ServiceId";
    public static final String UNI_PID		= "UNICA-PID";

    /** header solicitation theft value **/
    public static final String SUBSCRIPTION_KEY_VAL = "ff4f26c06fa34471ab30aea732df2ad5";
    public static final String UNI_USER_VAL	    = "QA";
    public static final String CONTENT_TYPE_VAL	    = "application/json";
    public static final String UNI_SERVICEID_VAL    = "UNICA-ServiceId-1234";
    public static final String UNI_PID_VAL	    = "UNICA-PID-765890";

    /** body solicitation theft value **/
    public static final String CXR		   = "CXR-Corte por robo";
    public static final String REASON_THEFT	   = "reasonTheft";
    public static final String LSHES		   = "LSHES";
    public static final String IMEI_KEY		   = "imei";
    public static final String CODE_UNIQUE	   = "codeUnique";
    public static final String QUANTITY		   = "1";
    public static final String UNKNOWN		   = "unknown";
    public static final String NOTIFICATION	   = "Notification";
    public static final String NOTIFICATION_STATUS = "notificationStatus";
    public static final String YES		   = "Si";

    private Constant() {
    }
}
