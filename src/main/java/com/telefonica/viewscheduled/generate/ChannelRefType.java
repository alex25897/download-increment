package com.telefonica.viewscheduled.generate;

import java.util.Objects;

import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Reference to a channel that can be queried with an API call.
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-19T16:24:38.761Z")

public class ChannelRefType {
    @JsonProperty("id")
    private String id = null;

    @JsonProperty("href")
    private String href = null;

    @JsonProperty("name")
    private String name = null;

    @JsonProperty("description")
    private String description = null;

    public ChannelRefType id(String id) {
	this.id = id;
	return this;
    }

    /**
     * Unique identifier of the channel
     * 
     * @return id
     **/
    @NotNull

    public String getId() {
	return id;
    }

    public void setId(String id) {
	this.id = id;
    }

    public ChannelRefType href(String href) {
	this.href = href;
	return this;
    }

    /**
     * URI where to query or perform actions on the channel
     * 
     * @return href
     **/
    @NotNull

    public String getHref() {
	return href;
    }

    public void setHref(String href) {
	this.href = href;
    }

    public ChannelRefType name(String name) {
	this.name = name;
	return this;
    }

    /**
     * Screen name of the channel
     * 
     * @return name
     **/

    public String getName() {
	return name;
    }

    public void setName(String name) {
	this.name = name;
    }

    public ChannelRefType description(String description) {
	this.description = description;
	return this;
    }

    /**
     * Description of the channel
     * 
     * @return description
     **/

    public String getDescription() {
	return description;
    }

    public void setDescription(String description) {
	this.description = description;
    }

    @Override
    public boolean equals(java.lang.Object o) {
	if (this == o) {
	    return true;
	}
	if (o == null || getClass() != o.getClass()) {
	    return false;
	}
	ChannelRefType channelRefType = (ChannelRefType) o;
	return Objects.equals(this.id, channelRefType.id) && Objects.equals(this.href, channelRefType.href)
		&& Objects.equals(this.name, channelRefType.name) && Objects.equals(this.description, channelRefType.description);
    }

    @Override
    public int hashCode() {
	return Objects.hash(id, href, name, description);
    }

    @Override
    public String toString() {
	StringBuilder sb = new StringBuilder();
	sb.append("class ChannelRefType {\n");

	sb.append("    id: ").append(toIndentedString(id)).append("\n");
	sb.append("    href: ").append(toIndentedString(href)).append("\n");
	sb.append("    name: ").append(toIndentedString(name)).append("\n");
	sb.append("    description: ").append(toIndentedString(description)).append("\n");
	sb.append("}");
	return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(java.lang.Object o) {
	if (o == null) {
	    return "null";
	}
	return o.toString().replace("\n", "\n    ");
    }
}
