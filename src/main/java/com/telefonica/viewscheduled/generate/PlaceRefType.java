package com.telefonica.viewscheduled.generate;

import java.util.Objects;

import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * PlaceRefType
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-19T16:24:38.761Z")

public class PlaceRefType {
    @JsonProperty("id")
    private String id = null;

    @JsonProperty("href")
    private String href = null;

    @JsonProperty("name")
    private String name = null;

    @JsonProperty("@referredType")
    private String referredType = null;

    public PlaceRefType id(String id) {
	this.id = id;
	return this;
    }

    /**
     * Unique identifier of the place
     * 
     * @return id
     **/

    @NotNull

    public String getId() {
	return id;
    }

    public void setId(String id) {
	this.id = id;
    }

    public PlaceRefType href(String href) {
	this.href = href;
	return this;
    }

    /**
     * A resource URI pointing to the resource in the OB that stores the place
     * detailed information. It can be queried using another API
     * 
     * @return href
     **/

    public String getHref() {
	return href;
    }

    public void setHref(String href) {
	this.href = href;
    }

    public PlaceRefType name(String name) {
	this.name = name;
	return this;
    }

    /**
     * Name of the place
     * 
     * @return name
     **/

    public String getName() {
	return name;
    }

    public void setName(String name) {
	this.name = name;
    }

    public PlaceRefType referredType(String referredType) {
	this.referredType = referredType;
	return this;
    }

    /**
     * Identifier of the type of the referred place
     * 
     * @return referredType
     **/

    public String getReferredType() {
	return referredType;
    }

    public void setReferredType(String referredType) {
	this.referredType = referredType;
    }

    @Override
    public boolean equals(java.lang.Object o) {
	if (this == o) {
	    return true;
	}
	if (o == null || getClass() != o.getClass()) {
	    return false;
	}
	PlaceRefType placeRefType = (PlaceRefType) o;
	return Objects.equals(this.id, placeRefType.id) && Objects.equals(this.href, placeRefType.href)
		&& Objects.equals(this.name, placeRefType.name) && Objects.equals(this.referredType, placeRefType.referredType);
    }

    @Override
    public int hashCode() {
	return Objects.hash(id, href, name, referredType);
    }

    @Override
    public String toString() {
	StringBuilder sb = new StringBuilder();
	sb.append("class PlaceRefType {\n");

	sb.append("    id: ").append(toIndentedString(id)).append("\n");
	sb.append("    href: ").append(toIndentedString(href)).append("\n");
	sb.append("    name: ").append(toIndentedString(name)).append("\n");
	sb.append("    referredType: ").append(toIndentedString(referredType)).append("\n");
	sb.append("}");
	return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(java.lang.Object o) {
	if (o == null) {
	    return "null";
	}
	return o.toString().replace("\n", "\n    ");
    }
}
