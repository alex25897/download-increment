package com.telefonica.viewscheduled.generate;

import java.util.Objects;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * RelatedProductType
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-19T16:24:38.761Z")

public class RelatedProductType {
    /**
     * A categorization of the relationship defined between the product referenced
     * and the original entity (bundle to indicate the related product is a
     * subproduct within the product resource to be created as part of the order;
     * reliesOn to indicate that the related product is another already existing
     * product required by the resource to be created as part of the order, for
     * instance to order an option within an existing product)
     */
    public enum TypeEnum {
	BUNDLE("bundle"),

	RELIESON("reliesOn");

	private String value;

	TypeEnum(String value) {
	    this.value = value;
	}

	@Override
	@JsonValue
	public String toString() {
	    return String.valueOf(value);
	}

	@JsonCreator
	public static TypeEnum fromValue(String text) {
	    for (TypeEnum b : TypeEnum.values()) {
		if (String.valueOf(b.value).equals(text)) {
		    return b;
		}
	    }
	    return null;
	}
    }

    @JsonProperty("type")
    private TypeEnum type = null;

    @JsonProperty("product")
    private ProductRefInfoType product = null;

    public RelatedProductType type(TypeEnum type) {
	this.type = type;
	return this;
    }

    /**
     * A categorization of the relationship defined between the product referenced
     * and the original entity (bundle to indicate the related product is a
     * subproduct within the product resource to be created as part of the order;
     * reliesOn to indicate that the related product is another already existing
     * product required by the resource to be created as part of the order, for
     * instance to order an option within an existing product)
     * 
     * @return type
     **/

    public TypeEnum getType() {
	return type;
    }

    public void setType(TypeEnum type) {
	this.type = type;
    }

    public RelatedProductType product(ProductRefInfoType product) {
	this.product = product;
	return this;
    }

    /**
     * Reference to the related product
     * 
     * @return product
     **/

    @NotNull

    @Valid

    public ProductRefInfoType getProduct() {
	return product;
    }

    public void setProduct(ProductRefInfoType product) {
	this.product = product;
    }

    @Override
    public boolean equals(java.lang.Object o) {
	if (this == o) {
	    return true;
	}
	if (o == null || getClass() != o.getClass()) {
	    return false;
	}
	RelatedProductType relatedProductType = (RelatedProductType) o;
	return Objects.equals(this.type, relatedProductType.type) && Objects.equals(this.product, relatedProductType.product);
    }

    @Override
    public int hashCode() {
	return Objects.hash(type, product);
    }

    @Override
    public String toString() {
	StringBuilder sb = new StringBuilder();
	sb.append("class RelatedProductType {\n");

	sb.append("    type: ").append(toIndentedString(type)).append("\n");
	sb.append("    product: ").append(toIndentedString(product)).append("\n");
	sb.append("}");
	return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(java.lang.Object o) {
	if (o == null) {
	    return "null";
	}
	return o.toString().replace("\n", "\n    ");
    }
}
