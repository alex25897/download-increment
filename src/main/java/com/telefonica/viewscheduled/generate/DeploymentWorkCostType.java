package com.telefonica.viewscheduled.generate;

import java.util.Objects;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * DeploymentWorkCostType
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-19T16:24:38.761Z")

public class DeploymentWorkCostType {
    @JsonProperty("name")
    private String name = null;

    @JsonProperty("price")
    private MoneyType price = null;

    /**
     * cost type
     */
    public enum TypeEnum {
	OPEX("opex"),

	CAPEX("capex");

	private String value;

	TypeEnum(String value) {
	    this.value = value;
	}

	@Override
	@JsonValue
	public String toString() {
	    return String.valueOf(value);
	}

	@JsonCreator
	public static TypeEnum fromValue(String text) {
	    for (TypeEnum b : TypeEnum.values()) {
		if (String.valueOf(b.value).equals(text)) {
		    return b;
		}
	    }
	    return null;
	}
    }

    @JsonProperty("type")
    private TypeEnum type = null;

    @JsonProperty("subject")
    private String subject = null;

    public DeploymentWorkCostType name(String name) {
	this.name = name;
	return this;
    }

    /**
     * Name of the cost
     * 
     * @return name
     **/

    public String getName() {
	return name;
    }

    public void setName(String name) {
	this.name = name;
    }

    public DeploymentWorkCostType price(MoneyType price) {
	this.price = price;
	return this;
    }

    /**
     * Price and currency of the work
     * 
     * @return price
     **/
    
    @NotNull

    @Valid

    public MoneyType getPrice() {
	return price;
    }

    public void setPrice(MoneyType price) {
	this.price = price;
    }

    public DeploymentWorkCostType type(TypeEnum type) {
	this.type = type;
	return this;
    }

    /**
     * cost type
     * 
     * @return type
     **/
    

    public TypeEnum getType() {
	return type;
    }

    public void setType(TypeEnum type) {
	this.type = type;
    }

    public DeploymentWorkCostType subject(String subject) {
	this.subject = subject;
	return this;
    }

    /**
     * What this cost actually pays for (e.g.: stock, workforce, etc.)
     * 
     * @return subject
     **/
    

    public String getSubject() {
	return subject;
    }

    public void setSubject(String subject) {
	this.subject = subject;
    }

    @Override
    public boolean equals(java.lang.Object o) {
	if (this == o) {
	    return true;
	}
	if (o == null || getClass() != o.getClass()) {
	    return false;
	}
	DeploymentWorkCostType deploymentWorkCostType = (DeploymentWorkCostType) o;
	return Objects.equals(this.name, deploymentWorkCostType.name) && Objects.equals(this.price, deploymentWorkCostType.price)
		&& Objects.equals(this.type, deploymentWorkCostType.type) && Objects.equals(this.subject, deploymentWorkCostType.subject);
    }

    @Override
    public int hashCode() {
	return Objects.hash(name, price, type, subject);
    }

    @Override
    public String toString() {
	StringBuilder sb = new StringBuilder();
	sb.append("class DeploymentWorkCostType {\n");

	sb.append("    name: ").append(toIndentedString(name)).append("\n");
	sb.append("    price: ").append(toIndentedString(price)).append("\n");
	sb.append("    type: ").append(toIndentedString(type)).append("\n");
	sb.append("    subject: ").append(toIndentedString(subject)).append("\n");
	sb.append("}");
	return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(java.lang.Object o) {
	if (o == null) {
	    return "null";
	}
	return o.toString().replace("\n", "\n    ");
    }
}
