package com.telefonica.viewscheduled.generate;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * ProductOrderUpdateRequestType
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-19T16:24:38.761Z")
public class ProductOrderUpdateRequestType {
    @JsonProperty("correlationId")
    private String correlationId = null;

    @JsonProperty("description")
    private String description = null;

    @JsonProperty("productOrderType")
    private String productOrderType = null;

    @JsonProperty("productOrderClass")
    @Valid
    private List<CategoryTreeType> productOrderClass = null;

    @JsonProperty("orderActionId")
    private String orderActionId = null;

    @JsonProperty("customerId")
    private String customerId = null;

    @JsonProperty("priority")
    private Integer priority = null;

    @JsonProperty("relatedParty")
    @Valid
    private List<RelatedPartyRefType> relatedParty = new ArrayList<RelatedPartyRefType>();

    @JsonProperty("relatedObject")
    @Valid
    private List<RelatedObjectType> relatedObject = null;

    @JsonProperty("channel")
    @Valid
    private List<ChannelRefType> channel = null;

    @JsonProperty("requestedStartDate")
    private OffsetDateTime requestedStartDate = null;

    @JsonProperty("requestedCompletionDate")
    private OffsetDateTime requestedCompletionDate = null;

    @JsonProperty("note")
    @Valid
    private List<NoteRequestType> note = null;

    @JsonProperty("orderItem")
    @Valid
    private List<OrderItemType> orderItem = new ArrayList<OrderItemType>();

    @JsonProperty("deploymentWorks")
    @Valid
    private List<DeploymentWorkType> deploymentWorks = null;

    @JsonProperty("additionalData")
    @Valid
    private List<KeyValueType> additionalData = null;

    @JsonProperty("callbackUrl")
    private String callbackUrl = null;

    /**
     * Sets (if allowed) the lifecycle status of the product order in the internal
     * ordering state machine of the processing system
     */
    public enum StatusEnum {
	NEW("new"),

	SUBMITTED("submitted"),

	REJECTED("rejected"),

	ACKNOWLEDGED("acknowledged"),

	IN_PROGRESS("in progress"),

	HELD("held"),

	CANCELLED("cancelled"),

	PARTIAL("partial"),

	PENDING("pending"),

	FAILED("failed"),

	COMPLETED("completed");

	private String value;

	StatusEnum(String value) {
	    this.value = value;
	}

	@Override
	@JsonValue
	public String toString() {
	    return String.valueOf(value);
	}

	@JsonCreator
	public static StatusEnum fromValue(String text) {
	    for (StatusEnum b : StatusEnum.values()) {
		if (String.valueOf(b.value).equals(text)) {
		    return b;
		}
	    }
	    return null;
	}
    }

    @JsonProperty("status")
    private StatusEnum status = null;

    @JsonProperty("subStatus")
    private String subStatus = null;

    @JsonProperty("statusReason")
    private String statusReason = null;

    public ProductOrderUpdateRequestType correlationId(String correlationId) {
	this.correlationId = correlationId;
	return this;
    }

    /**
     * Unique identifier for the order created within the client, used to
     * synchronize and map internal identifiers between server and client. Notice
     * that in the TMForum API version 16.5 this parameter is named externalId
     * 
     * @return correlationId
     **/

    public String getCorrelationId() {
	return correlationId;
    }

    public void setCorrelationId(String correlationId) {
	this.correlationId = correlationId;
    }

    public ProductOrderUpdateRequestType description(String description) {
	this.description = description;
	return this;
    }

    /**
     * Some text providing a user-friendly detailed description of the product order
     * 
     * @return description
     **/

    public String getDescription() {
	return description;
    }

    public void setDescription(String description) {
	this.description = description;
    }

    public ProductOrderUpdateRequestType productOrderType(String productOrderType) {
	this.productOrderType = productOrderType;
	return this;
    }

    /**
     * Indication of the type of order registered. Supported values are
     * implementation and application specific. Used to categorize the order from a
     * business perspective that can be useful for the OM system (e.g.: enterprise,
     * residential, ...). Notice that in the TMForum API version 16.5 this parameter
     * is named category
     * 
     * @return productOrderType
     **/

    public String getProductOrderType() {
	return productOrderType;
    }

    public void setProductOrderType(String productOrderType) {
	this.productOrderType = productOrderType;
    }

    public ProductOrderUpdateRequestType productOrderClass(List<CategoryTreeType> productOrderClass) {
	this.productOrderClass = productOrderClass;
	return this;
    }

    public ProductOrderUpdateRequestType addProductOrderClassItem(CategoryTreeType productOrderClassItem) {
	if (this.productOrderClass == null) {
	    this.productOrderClass = new ArrayList<CategoryTreeType>();
	}
	this.productOrderClass.add(productOrderClassItem);
	return this;
    }

    /**
     * A class defining the categorization of the product order. A product order may
     * belong to more than one class. A standard list of classes
     * (e.g.:Video,Audio,Adult, etc). Each implementation may define its own classes
     * 
     * @return productOrderClass
     **/

    @Valid

    public List<CategoryTreeType> getProductOrderClass() {
	return productOrderClass;
    }

    public void setProductOrderClass(List<CategoryTreeType> productOrderClass) {
	this.productOrderClass = productOrderClass;
    }

    public ProductOrderUpdateRequestType orderActionId(String orderActionId) {
	this.orderActionId = orderActionId;
	return this;
    }

    /**
     * Typically orders can be decomposed in order actions
     * 
     * @return orderActionId
     **/

    public String getOrderActionId() {
	return orderActionId;
    }

    public void setOrderActionId(String orderActionId) {
	this.orderActionId = orderActionId;
    }

    public ProductOrderUpdateRequestType customerId(String customerId) {
	this.customerId = customerId;
	return this;
    }

    /**
     * Unique identifier of the customer
     * 
     * @return customerId
     **/

    public String getCustomerId() {
	return customerId;
    }

    public void setCustomerId(String customerId) {
	this.customerId = customerId;
    }

    public ProductOrderUpdateRequestType priority(Integer priority) {
	this.priority = priority;
	return this;
    }

    /**
     * Indication of the priority considered by the originator of the order. An
     * example of possible values are: 3 (low), 2 (medium), 1 (high), 0 (critical)
     * 
     * @return priority
     **/

    public Integer getPriority() {
	return priority;
    }

    public void setPriority(Integer priority) {
	this.priority = priority;
    }

    public ProductOrderUpdateRequestType relatedParty(List<RelatedPartyRefType> relatedParty) {
	this.relatedParty = relatedParty;
	return this;
    }

    public ProductOrderUpdateRequestType addRelatedPartyItem(RelatedPartyRefType relatedPartyItem) {
	this.relatedParty.add(relatedPartyItem);
	return this;
    }

    /**
     * List of individuals (e.g.: support agent, system impacted, reviewer, u
     * associated to the order
     * 
     * @return relatedParty
     **/

    @NotNull

    @Valid

    public List<RelatedPartyRefType> getRelatedParty() {
	return relatedParty;
    }

    public void setRelatedParty(List<RelatedPartyRefType> relatedParty) {
	this.relatedParty = relatedParty;
    }

    public ProductOrderUpdateRequestType relatedObject(List<RelatedObjectType> relatedObject) {
	this.relatedObject = relatedObject;
	return this;
    }

    public ProductOrderUpdateRequestType addRelatedObjectItem(RelatedObjectType relatedObjectItem) {
	if (this.relatedObject == null) {
	    this.relatedObject = new ArrayList<RelatedObjectType>();
	}
	this.relatedObject.add(relatedObjectItem);
	return this;
    }

    /**
     * List of Objects or resources associated to an order (e.g.: framework
     * agreement, opportunity, u
     * 
     * @return relatedObject
     **/

    @Valid

    public List<RelatedObjectType> getRelatedObject() {
	return relatedObject;
    }

    public void setRelatedObject(List<RelatedObjectType> relatedObject) {
	this.relatedObject = relatedObject;
    }

    public ProductOrderUpdateRequestType channel(List<ChannelRefType> channel) {
	this.channel = channel;
	return this;
    }

    public ProductOrderUpdateRequestType addChannelItem(ChannelRefType channelItem) {
	if (this.channel == null) {
	    this.channel = new ArrayList<ChannelRefType>();
	}
	this.channel.add(channelItem);
	return this;
    }

    /**
     * Defines the channel that is used to trigger the order. This element is
     * defined as an array in order to allow the option to associate an order to
     * multiple channels
     * 
     * @return channel
     **/

    @Valid

    public List<ChannelRefType> getChannel() {
	return channel;
    }

    public void setChannel(List<ChannelRefType> channel) {
	this.channel = channel;
    }

    public ProductOrderUpdateRequestType requestedStartDate(OffsetDateTime requestedStartDate) {
	this.requestedStartDate = requestedStartDate;
	return this;
    }

    /**
     * Order start date wished by the requestor (e.g.: date when delivery process
     * should begin). This could be required in order to manage SLA compliancy
     * 
     * @return requestedStartDate
     **/

    @Valid

    public OffsetDateTime getRequestedStartDate() {
	return requestedStartDate;
    }

    public void setRequestedStartDate(OffsetDateTime requestedStartDate) {
	this.requestedStartDate = requestedStartDate;
    }

    public ProductOrderUpdateRequestType requestedCompletionDate(OffsetDateTime requestedCompletionDate) {
	this.requestedCompletionDate = requestedCompletionDate;
	return this;
    }

    /**
     * Requested delivery date from the requestor perspective (e.g.: date when
     * delivery of all the items within an order should be complete). This could be
     * required in order to manage SLA compliancy
     * 
     * @return requestedCompletionDate
     **/

    @Valid

    public OffsetDateTime getRequestedCompletionDate() {
	return requestedCompletionDate;
    }

    public void setRequestedCompletionDate(OffsetDateTime requestedCompletionDate) {
	this.requestedCompletionDate = requestedCompletionDate;
    }

    public ProductOrderUpdateRequestType note(List<NoteRequestType> note) {
	this.note = note;
	return this;
    }

    public ProductOrderUpdateRequestType addNoteItem(NoteRequestType noteItem) {
	if (this.note == null) {
	    this.note = new ArrayList<NoteRequestType>();
	}
	this.note.add(noteItem);
	return this;
    }

    /**
     * List of notes to be added as part of the creation of the order
     * 
     * @return note
     **/

    @Valid

    public List<NoteRequestType> getNote() {
	return note;
    }

    public void setNote(List<NoteRequestType> note) {
	this.note = note;
    }

    public ProductOrderUpdateRequestType orderItem(List<OrderItemType> orderItem) {
	this.orderItem = orderItem;
	return this;
    }

    public ProductOrderUpdateRequestType addOrderItemItem(OrderItemType orderItemItem) {
	this.orderItem.add(orderItemItem);
	return this;
    }

    /**
     * List of individual items included in the order
     * 
     * @return orderItem
     **/

    @NotNull

    @Valid
    @Size(min = 1)
    public List<OrderItemType> getOrderItem() {
	return orderItem;
    }

    public void setOrderItem(List<OrderItemType> orderItem) {
	this.orderItem = orderItem;
    }

    public ProductOrderUpdateRequestType deploymentWorks(List<DeploymentWorkType> deploymentWorks) {
	this.deploymentWorks = deploymentWorks;
	return this;
    }

    public ProductOrderUpdateRequestType addDeploymentWorksItem(DeploymentWorkType deploymentWorksItem) {
	if (this.deploymentWorks == null) {
	    this.deploymentWorks = new ArrayList<DeploymentWorkType>();
	}
	this.deploymentWorks.add(deploymentWorksItem);
	return this;
    }

    /**
     * When an order item implies some sort of work such as cable deployments,
     * towers, etc. Typically used in B2B, since in B2C end customers are abstracted
     * from these tasks
     * 
     * @return deploymentWorks
     **/

    @Valid

    public List<DeploymentWorkType> getDeploymentWorks() {
	return deploymentWorks;
    }

    public void setDeploymentWorks(List<DeploymentWorkType> deploymentWorks) {
	this.deploymentWorks = deploymentWorks;
    }

    public ProductOrderUpdateRequestType additionalData(List<KeyValueType> additionalData) {
	this.additionalData = additionalData;
	return this;
    }

    public ProductOrderUpdateRequestType addAdditionalDataItem(KeyValueType additionalDataItem) {
	if (this.additionalData == null) {
	    this.additionalData = new ArrayList<KeyValueType>();
	}
	this.additionalData.add(additionalDataItem);
	return this;
    }

    /**
     * Any further information needed by the server to fill the entity definition.
     * It is recommended not to use this parameter and request new information
     * elements to be added in the structure definition. Next releases of the T-Open
     * API will not include support for this additionalData parameter because it has
     * been detected that the extensibility function is not helping the stability of
     * the standard definition of APIs
     * 
     * @return additionalData
     **/

    @Valid

    public List<KeyValueType> getAdditionalData() {
	return additionalData;
    }

    public void setAdditionalData(List<KeyValueType> additionalData) {
	this.additionalData = additionalData;
    }

    public ProductOrderUpdateRequestType callbackUrl(String callbackUrl) {
	this.callbackUrl = callbackUrl;
	return this;
    }

    /**
     * An URL that will allow the server to report asynchronously any further
     * modification defined by the server on any of the parameters defining a
     * product order previously created. It should be ignored if a provisioned
     * callbackUrl exists
     * 
     * @return callbackUrl
     **/

    public String getCallbackUrl() {
	return callbackUrl;
    }

    public void setCallbackUrl(String callbackUrl) {
	this.callbackUrl = callbackUrl;
    }

    public ProductOrderUpdateRequestType status(StatusEnum status) {
	this.status = status;
	return this;
    }

    /**
     * Sets (if allowed) the lifecycle status of the product order in the internal
     * ordering state machine of the processing system
     * 
     * @return status
     **/

    @NotNull

    public StatusEnum getStatus() {
	return status;
    }

    public void setStatus(StatusEnum status) {
	this.status = status;
    }

    public ProductOrderUpdateRequestType subStatus(String subStatus) {
	this.subStatus = subStatus;
	return this;
    }

    /**
     * Substatus in order to define a second status level
     * 
     * @return subStatus
     **/

    public String getSubStatus() {
	return subStatus;
    }

    public void setSubStatus(String subStatus) {
	this.subStatus = subStatus;
    }

    public ProductOrderUpdateRequestType statusReason(String statusReason) {
	this.statusReason = statusReason;
	return this;
    }

    /**
     * Reasoning for the status/substatus, for instance reasoning for an order
     * process failed
     * 
     * @return statusReason
     **/

    public String getStatusReason() {
	return statusReason;
    }

    public void setStatusReason(String statusReason) {
	this.statusReason = statusReason;
    }

    @Override
    public boolean equals(java.lang.Object o) {
	if (this == o) {
	    return true;
	}
	if (o == null || getClass() != o.getClass()) {
	    return false;
	}
	ProductOrderUpdateRequestType productOrderUpdateRequestType = (ProductOrderUpdateRequestType) o;
	return Objects.equals(this.correlationId, productOrderUpdateRequestType.correlationId)
		&& Objects.equals(this.description, productOrderUpdateRequestType.description)
		&& Objects.equals(this.productOrderType, productOrderUpdateRequestType.productOrderType)
		&& Objects.equals(this.productOrderClass, productOrderUpdateRequestType.productOrderClass)
		&& Objects.equals(this.orderActionId, productOrderUpdateRequestType.orderActionId)
		&& Objects.equals(this.customerId, productOrderUpdateRequestType.customerId)
		&& Objects.equals(this.priority, productOrderUpdateRequestType.priority)
		&& Objects.equals(this.relatedParty, productOrderUpdateRequestType.relatedParty)
		&& Objects.equals(this.relatedObject, productOrderUpdateRequestType.relatedObject)
		&& Objects.equals(this.channel, productOrderUpdateRequestType.channel)
		&& Objects.equals(this.requestedStartDate, productOrderUpdateRequestType.requestedStartDate)
		&& Objects.equals(this.requestedCompletionDate, productOrderUpdateRequestType.requestedCompletionDate)
		&& Objects.equals(this.note, productOrderUpdateRequestType.note)
		&& Objects.equals(this.orderItem, productOrderUpdateRequestType.orderItem)
		&& Objects.equals(this.deploymentWorks, productOrderUpdateRequestType.deploymentWorks)
		&& Objects.equals(this.additionalData, productOrderUpdateRequestType.additionalData)
		&& Objects.equals(this.callbackUrl, productOrderUpdateRequestType.callbackUrl)
		&& Objects.equals(this.status, productOrderUpdateRequestType.status)
		&& Objects.equals(this.subStatus, productOrderUpdateRequestType.subStatus)
		&& Objects.equals(this.statusReason, productOrderUpdateRequestType.statusReason);
    }

    @Override
    public int hashCode() {
	return Objects.hash(correlationId, description, productOrderType, productOrderClass, orderActionId, customerId, priority,
		relatedParty, relatedObject, channel, requestedStartDate, requestedCompletionDate, note, orderItem, deploymentWorks,
		additionalData, callbackUrl, status, subStatus, statusReason);
    }

    @Override
    public String toString() {
	StringBuilder sb = new StringBuilder();
	sb.append("class ProductOrderUpdateRequestType {\n");

	sb.append("    correlationId: ").append(toIndentedString(correlationId)).append("\n");
	sb.append("    description: ").append(toIndentedString(description)).append("\n");
	sb.append("    productOrderType: ").append(toIndentedString(productOrderType)).append("\n");
	sb.append("    productOrderClass: ").append(toIndentedString(productOrderClass)).append("\n");
	sb.append("    orderActionId: ").append(toIndentedString(orderActionId)).append("\n");
	sb.append("    customerId: ").append(toIndentedString(customerId)).append("\n");
	sb.append("    priority: ").append(toIndentedString(priority)).append("\n");
	sb.append("    relatedParty: ").append(toIndentedString(relatedParty)).append("\n");
	sb.append("    relatedObject: ").append(toIndentedString(relatedObject)).append("\n");
	sb.append("    channel: ").append(toIndentedString(channel)).append("\n");
	sb.append("    requestedStartDate: ").append(toIndentedString(requestedStartDate)).append("\n");
	sb.append("    requestedCompletionDate: ").append(toIndentedString(requestedCompletionDate)).append("\n");
	sb.append("    note: ").append(toIndentedString(note)).append("\n");
	sb.append("    orderItem: ").append(toIndentedString(orderItem)).append("\n");
	sb.append("    deploymentWorks: ").append(toIndentedString(deploymentWorks)).append("\n");
	sb.append("    additionalData: ").append(toIndentedString(additionalData)).append("\n");
	sb.append("    callbackUrl: ").append(toIndentedString(callbackUrl)).append("\n");
	sb.append("    status: ").append(toIndentedString(status)).append("\n");
	sb.append("    subStatus: ").append(toIndentedString(subStatus)).append("\n");
	sb.append("    statusReason: ").append(toIndentedString(statusReason)).append("\n");
	sb.append("}");
	return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(java.lang.Object o) {
	if (o == null) {
	    return "null";
	}
	return o.toString().replace("\n", "\n    ");
    }
}
