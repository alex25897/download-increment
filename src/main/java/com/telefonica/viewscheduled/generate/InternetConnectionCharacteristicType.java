package com.telefonica.viewscheduled.generate;

import java.util.Objects;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * InternetConnectionCharacteristicType
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-19T16:24:38.761Z")

public class InternetConnectionCharacteristicType {
    @JsonProperty("connection")
    private ConnectionType connection = null;

    public InternetConnectionCharacteristicType connection(ConnectionType connection) {
	this.connection = connection;
	return this;
    }

    /**
     * Information for broadband/internet type products, providing details on
     * connections characteristics
     * 
     * @return connection
     **/

    @NotNull

    @Valid

    public ConnectionType getConnection() {
	return connection;
    }

    public void setConnection(ConnectionType connection) {
	this.connection = connection;
    }

    @Override
    public boolean equals(java.lang.Object o) {
	if (this == o) {
	    return true;
	}
	if (o == null || getClass() != o.getClass()) {
	    return false;
	}
	InternetConnectionCharacteristicType internetConnectionCharacteristicType = (InternetConnectionCharacteristicType) o;
	return Objects.equals(this.connection, internetConnectionCharacteristicType.connection);
    }

    @Override
    public int hashCode() {
	return Objects.hash(connection);
    }

    @Override
    public String toString() {
	StringBuilder sb = new StringBuilder();
	sb.append("class InternetConnectionCharacteristicType {\n");

	sb.append("    connection: ").append(toIndentedString(connection)).append("\n");
	sb.append("}");
	return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(java.lang.Object o) {
	if (o == null) {
	    return "null";
	}
	return o.toString().replace("\n", "\n    ");
    }
}
