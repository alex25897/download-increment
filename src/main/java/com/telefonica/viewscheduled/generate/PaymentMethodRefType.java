package com.telefonica.viewscheduled.generate;

import java.util.Objects;

import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Reference of a payment method stored somewhere else
 */

@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-19T16:24:38.761Z")

public class PaymentMethodRefType {
    @JsonProperty("id")
    private String id = null;

    @JsonProperty("href")
    private String href = null;

    /**
     * Referred payment method type
     */
    public enum ReferredTypeEnum {
	CASH("cash"),

	DIGITALWALLET("digitalWallet"),

	TOKENIZEDCARD("tokenizedCard"),

	BANKACCOUNTTRANSFER("bankAccountTransfer"),

	BANKACCOUNTDEBIT("bankAccountDebit"),

	BANKCARD("bankCard"),

	ACCOUNT("account"),

	BUCKET("bucket"),

	VOUCHER("voucher"),

	CHECK("check"),

	LOYALTYACCOUNT("loyaltyAccount");

	private String value;

	ReferredTypeEnum(String value) {
	    this.value = value;
	}

	@Override
	@JsonValue
	public String toString() {
	    return String.valueOf(value);
	}

	@JsonCreator
	public static ReferredTypeEnum fromValue(String text) {
	    for (ReferredTypeEnum b : ReferredTypeEnum.values()) {
		if (String.valueOf(b.value).equals(text)) {
		    return b;
		}
	    }
	    return null;
	}
    }

    @JsonProperty("@referredType")
    private ReferredTypeEnum referredType = null;

    @JsonProperty("name")
    private String name = null;

    @JsonProperty("description")
    private String description = null;

    public PaymentMethodRefType id(String id) {
	this.id = id;
	return this;
    }

    /**
     * Unique identifier within the OB for he payment method
     * 
     * @return id
     **/

    @NotNull

    public String getId() {
	return id;
    }

    public void setId(String id) {
	this.id = id;
    }

    public PaymentMethodRefType href(String href) {
	this.href = href;
	return this;
    }

    /**
     * A resource URI pointing to the resource in the OB that stores the account
     * detailed information e.g.:paymentMethods/v1/paymentMethods/{paymentMethodId}
     * 
     * @return href
     **/

    public String getHref() {
	return href;
    }

    public void setHref(String href) {
	this.href = href;
    }

    public PaymentMethodRefType referredType(ReferredTypeEnum referredType) {
	this.referredType = referredType;
	return this;
    }

    /**
     * Referred payment method type
     * 
     * @return referredType
     **/

    public ReferredTypeEnum getReferredType() {
	return referredType;
    }

    public void setReferredType(ReferredTypeEnum referredType) {
	this.referredType = referredType;
    }

    public PaymentMethodRefType name(String name) {
	this.name = name;
	return this;
    }

    /**
     * Name of the payment method stored in the server associated to the account
     * 
     * @return name
     **/

    public String getName() {
	return name;
    }

    public void setName(String name) {
	this.name = name;
    }

    public PaymentMethodRefType description(String description) {
	this.description = description;
	return this;
    }

    /**
     * Description of the payment method in the server associated to the account
     * 
     * @return description
     **/

    public String getDescription() {
	return description;
    }

    public void setDescription(String description) {
	this.description = description;
    }

    @Override
    public boolean equals(java.lang.Object o) {
	if (this == o) {
	    return true;
	}
	if (o == null || getClass() != o.getClass()) {
	    return false;
	}
	PaymentMethodRefType paymentMethodRefType = (PaymentMethodRefType) o;
	return Objects.equals(this.id, paymentMethodRefType.id) && Objects.equals(this.href, paymentMethodRefType.href)
		&& Objects.equals(this.referredType, paymentMethodRefType.referredType)
		&& Objects.equals(this.name, paymentMethodRefType.name)
		&& Objects.equals(this.description, paymentMethodRefType.description);
    }

    @Override
    public int hashCode() {
	return Objects.hash(id, href, referredType, name, description);
    }

    @Override
    public String toString() {
	StringBuilder sb = new StringBuilder();
	sb.append("class PaymentMethodRefType {\n");

	sb.append("    id: ").append(toIndentedString(id)).append("\n");
	sb.append("    href: ").append(toIndentedString(href)).append("\n");
	sb.append("    referredType: ").append(toIndentedString(referredType)).append("\n");
	sb.append("    name: ").append(toIndentedString(name)).append("\n");
	sb.append("    description: ").append(toIndentedString(description)).append("\n");
	sb.append("}");
	return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(java.lang.Object o) {
	if (o == null) {
	    return "null";
	}
	return o.toString().replace("\n", "\n    ");
    }
}
