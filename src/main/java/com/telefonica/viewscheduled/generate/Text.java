package com.telefonica.viewscheduled.generate;

import java.util.Objects;

import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Text
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-19T16:24:38.761Z")

public class Text extends ProductCharacteristicType {
    @JsonProperty("name")
    private String name = null;

    @JsonProperty("value")
    private String value = null;

    @JsonProperty("category")
    private String category = null;

    @JsonProperty("type")
    private String type = null;

    public Text name(String name) {
	this.name = name;
	return this;
    }

    /**
     * First part of the textual descriptive characteristic
     * 
     * @return name
     **/

    @NotNull

    public String getName() {
	return name;
    }

    public void setName(String name) {
	this.name = name;
    }

    public Text value(String value) {
	this.value = value;
	return this;
    }

    /**
     * Second part of the textual descriptive characteristic
     * 
     * @return value
     **/

    @NotNull

    public String getValue() {
	return value;
    }

    public void setValue(String value) {
	this.value = value;
    }

    public Text category(String category) {
	this.category = category;
	return this;
    }

    /**
     * Use this to group characteristics (s.g.: commercial, descriptive,
     * technical...)
     * 
     * @return category
     **/

    public String getCategory() {
	return category;
    }

    public void setCategory(String category) {
	this.category = category;
    }

    public Text type(String type) {
	this.type = type;
	return this;
    }

    /**
     * In case text characteristics have different types (e.g.: printable, not
     * printable, etc.)
     * 
     * @return type
     **/

    
 
    public void setType(String type) {
	this.type = type;
    }

    public String geType() {
        return type;
    }

    @Override
    public boolean equals(java.lang.Object o) {
	if (this == o) {
	    return true;
	}
	if (o == null || getClass() != o.getClass()) {
	    return false;
	}
	Text text = (Text) o;
	return Objects.equals(this.name, text.name) && Objects.equals(this.value, text.value)
		&& Objects.equals(this.category, text.category) && Objects.equals(this.type, text.type) && super.equals(o);
    }

    @Override
    public int hashCode() {
	return Objects.hash(name, value, category, type, super.hashCode());
    }

    @Override
    public String toString() {
	StringBuilder sb = new StringBuilder();
	sb.append("class Text {\n");
	sb.append("    ").append(toIndentedString(super.toString())).append("\n");
	sb.append("    name: ").append(toIndentedString(name)).append("\n");
	sb.append("    value: ").append(toIndentedString(value)).append("\n");
	sb.append("    category: ").append(toIndentedString(category)).append("\n");
	sb.append("    type: ").append(toIndentedString(type)).append("\n");
	sb.append("}");
	return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(java.lang.Object o) {
	if (o == null) {
	    return "null";
	}
	return o.toString().replace("\n", "\n    ");
    }
}
