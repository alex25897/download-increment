package com.telefonica.viewscheduled.generate;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * OrderItemType
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-19T16:24:38.761Z")

public class OrderItemType {
    @JsonProperty("id")
    private String id = null;

    @JsonProperty("description")
    private String description = null;

    /**
     * Indicates the order-related action requested as part of the order item (add a
     * new product to an account, modify an existing product or remove a product
     * from an account)
     */
    public enum ActionEnum {
	ADD("add"),

	MODIFY("modify"),

	DELETE("delete"),

	NO_CHANGE("no_change"),

	IMPLICIT("implicit"),

	UNDEFINED("undefined");

	private String value;

	ActionEnum(String value) {
	    this.value = value;
	}

	@Override
	@JsonValue
	public String toString() {
	    return String.valueOf(value);
	}

	@JsonCreator
	public static ActionEnum fromValue(String text) {
	    for (ActionEnum b : ActionEnum.values()) {
		if (String.valueOf(b.value).equals(text)) {
		    return b;
		}
	    }
	    return null;
	}
    }

    @JsonProperty("action")
    private ActionEnum action = null;

    @JsonProperty("parent")
    private String parent = null;

    @JsonProperty("productOffering")
    private ProductOfferingRefType productOffering = null;

    @JsonProperty("product")
    private ProductRefInfoType product = null;

    @JsonProperty("quantity")
    private String quantity = null;

    @JsonProperty("minQuantity")
    private String minQuantity = null;

    @JsonProperty("maxQuantity")
    private String maxQuantity = null;

    @JsonProperty("orderItemPrice")
    @Valid
    private List<ComponentProdPriceType> orderItemPrice = null;

    @JsonProperty("billingAccount")
    @Valid
    private List<AccountRefType> billingAccount = null;

    @JsonProperty("paymentMethod")
    private PaymentMethodRefType paymentMethod = null;

    /**
     * Tracks the lifecycle status of the product order item in the internal
     * ordering state machine of the processing system
     */
    public enum StatusEnum {
	NEW("new"),

	SUBMITTED("submitted"),

	REJECTED("rejected"),

	ACKNOWLEDGED("acknowledged"),

	IN_PROGRESS("in progress"),

	HELD("held"),

	CANCELLED("cancelled"),

	PENDING("pending"),

	FAILED("failed"),

	COMPLETED("completed");

	private String value;

	StatusEnum(String value) {
	    this.value = value;
	}

	@Override
	@JsonValue
	public String toString() {
	    return String.valueOf(value);
	}

	@JsonCreator
	public static StatusEnum fromValue(String text) {
	    for (StatusEnum b : StatusEnum.values()) {
		if (String.valueOf(b.value).equals(text)) {
		    return b;
		}
	    }
	    return null;
	}
    }

    @JsonProperty("status")
    private StatusEnum status = null;

    @JsonProperty("statusReason")
    private String statusReason = null;

    /**
     * The status to which the product is set (for responses) or has to be set (for
     * requests)
     */
    public enum ActivationStatusEnum {
	NEW("new"),

	CREATED("created"),

	ACTIVE("active"),

	ABORTED("aborted"),

	SUSPENDED("suspended"),

	CANCELLED("cancelled"),

	TERMINATED("terminated"),

	PENDING("pending"),

	INFORMATIONAL("informational"),

	TRIAL("trial"),

	KEEP("keep");

	private String value;

	ActivationStatusEnum(String value) {
	    this.value = value;
	}

	@Override
	@JsonValue
	public String toString() {
	    return String.valueOf(value);
	}

	@JsonCreator
	public static ActivationStatusEnum fromValue(String text) {
	    for (ActivationStatusEnum b : ActivationStatusEnum.values()) {
		if (String.valueOf(b.value).equals(text)) {
		    return b;
		}
	    }
	    return null;
	}
    }

    @JsonProperty("activationStatus")
    private ActivationStatusEnum activationStatus = null;

    @JsonProperty("validFor")
    private TimePeriodType validFor = null;

    @JsonProperty("isAutoRenew")
    private Boolean isAutoRenew = null;

    @JsonProperty("recurringPeriod")
    private String recurringPeriod = null;

    @JsonProperty("nrOfPeriods")
    private Integer nrOfPeriods = null;

    @JsonProperty("additionalData")
    @Valid
    private List<KeyValueType> additionalData = null;

    public OrderItemType id(String id) {
	this.id = id;
	return this;
    }

    /**
     * Unique identifier of the order item. It might be the same as a product ID in
     * case order items contain only one product
     * 
     * @return id
     **/

    public String getId() {
	return id;
    }

    public void setId(String id) {
	this.id = id;
    }

    public OrderItemType description(String description) {
	this.description = description;
	return this;
    }

    /**
     * Some text providing a user-friendly detailed description of the product order
     * item
     * 
     * @return description
     **/

    public String getDescription() {
	return description;
    }

    public void setDescription(String description) {
	this.description = description;
    }

    public OrderItemType action(ActionEnum action) {
	this.action = action;
	return this;
    }

    /**
     * Indicates the order-related action requested as part of the order item (add a
     * new product to an account, modify an existing product or remove a product
     * from an account)
     * 
     * @return action
     **/

    @NotNull

    public ActionEnum getAction() {
	return action;
    }

    public void setAction(ActionEnum action) {
	this.action = action;
    }

    public OrderItemType parent(String parent) {
	this.parent = parent;
	return this;
    }

    /**
     * Id of the parent in case there is a hierarchical relationship between order
     * items, i.e. order items may have children. If it's not included, the order
     * item is considered to be at root level.
     * 
     * @return parent
     **/

    public String getParent() {
	return parent;
    }

    public void setParent(String parent) {
	this.parent = parent;
    }

    public OrderItemType productOffering(ProductOfferingRefType productOffering) {
	this.productOffering = productOffering;
	return this;
    }

    /**
     * Reference to the Offering Identifier within the product catalogue that
     * includes the description of the products that are requested as part of the
     * order
     * 
     * @return productOffering
     **/

    @Valid

    public ProductOfferingRefType getProductOffering() {
	return productOffering;
    }

    public void setProductOffering(ProductOfferingRefType productOffering) {
	this.productOffering = productOffering;
    }

    public OrderItemType product(ProductRefInfoType product) {
	this.product = product;
	return this;
    }

    /**
     * Definition of the product requested to be added (or to be modified) to an
     * account as part of the order item
     * 
     * @return product
     **/

    @Valid

    public ProductRefInfoType getProduct() {
	return product;
    }

    public void setProduct(ProductRefInfoType product) {
	this.product = product;
    }

    public OrderItemType quantity(String quantity) {
	this.quantity = quantity;
	return this;
    }

    /**
     * Number of units ordered of a given product (e.g.: number of instances to be
     * created)
     * 
     * @return quantity
     **/

    @NotNull

    public String getQuantity() {
	return quantity;
    }

    public void setQuantity(String quantity) {
	this.quantity = quantity;
    }

    public OrderItemType minQuantity(String minQuantity) {
	this.minQuantity = minQuantity;
	return this;
    }

    /**
     * This parameter also allows defining the minimum number of units that could be
     * instantiated when the order includes the flexibility of post-order activation
     * of components to be charged per use
     * 
     * @return minQuantity
     **/

    public String getMinQuantity() {
	return minQuantity;
    }

    public void setMinQuantity(String minQuantity) {
	this.minQuantity = minQuantity;
    }

    public OrderItemType maxQuantity(String maxQuantity) {
	this.maxQuantity = maxQuantity;
	return this;
    }

    /**
     * This parameter also allows defining the maximum number of units that could be
     * instantiated when the order includes the flexibility of post-order activation
     * of components to be charged per use
     * 
     * @return maxQuantity
     **/

    public String getMaxQuantity() {
	return maxQuantity;
    }

    public void setMaxQuantity(String maxQuantity) {
	this.maxQuantity = maxQuantity;
    }

    public OrderItemType orderItemPrice(List<ComponentProdPriceType> orderItemPrice) {
	this.orderItemPrice = orderItemPrice;
	return this;
    }

    public OrderItemType addOrderItemPriceItem(ComponentProdPriceType orderItemPriceItem) {
	if (this.orderItemPrice == null) {
	    this.orderItemPrice = new ArrayList<ComponentProdPriceType>();
	}
	this.orderItemPrice.add(orderItemPriceItem);
	return this;
    }

    /**
     * List of price models applied to the product offering requested in the order
     * item
     * 
     * @return orderItemPrice
     **/

    @Valid

    public List<ComponentProdPriceType> getOrderItemPrice() {
	return orderItemPrice;
    }

    public void setOrderItemPrice(List<ComponentProdPriceType> orderItemPrice) {
	this.orderItemPrice = orderItemPrice;
    }

    public OrderItemType billingAccount(List<AccountRefType> billingAccount) {
	this.billingAccount = billingAccount;
	return this;
    }

    public OrderItemType addBillingAccountItem(AccountRefType billingAccountItem) {
	if (this.billingAccount == null) {
	    this.billingAccount = new ArrayList<AccountRefType>();
	}
	this.billingAccount.add(billingAccountItem);
	return this;
    }

    /**
     * Reference to the billing accounts that are impacted by the order item. This
     * attribute is an array to allow scenarios where a product or service is shared
     * for payment between different accounts
     * 
     * @return billingAccount
     **/

    @Valid

    public List<AccountRefType> getBillingAccount() {
	return billingAccount;
    }

    public void setBillingAccount(List<AccountRefType> billingAccount) {
	this.billingAccount = billingAccount;
    }

    public OrderItemType paymentMethod(PaymentMethodRefType paymentMethod) {
	this.paymentMethod = paymentMethod;
	return this;
    }

    /**
     * Reference to the type of payment method to be used for the product or service
     * ordered on the corresponding account
     * 
     * @return paymentMethod
     **/

    @Valid

    public PaymentMethodRefType getPaymentMethod() {
	return paymentMethod;
    }

    public void setPaymentMethod(PaymentMethodRefType paymentMethod) {
	this.paymentMethod = paymentMethod;
    }

    public OrderItemType status(StatusEnum status) {
	this.status = status;
	return this;
    }

    /**
     * Tracks the lifecycle status of the product order item in the internal
     * ordering state machine of the processing system
     * 
     * @return status
     **/

    public StatusEnum getStatus() {
	return status;
    }

    public void setStatus(StatusEnum status) {
	this.status = status;
    }

    public OrderItemType statusReason(String statusReason) {
	this.statusReason = statusReason;
	return this;
    }

    /**
     * Reasoning for the status, for instance reasoning for an order item process
     * failed
     * 
     * @return statusReason
     **/

    public String getStatusReason() {
	return statusReason;
    }

    public void setStatusReason(String statusReason) {
	this.statusReason = statusReason;
    }

    public OrderItemType activationStatus(ActivationStatusEnum activationStatus) {
	this.activationStatus = activationStatus;
	return this;
    }

    /**
     * The status to which the product is set (for responses) or has to be set (for
     * requests)
     * 
     * @return activationStatus
     **/

    public ActivationStatusEnum getActivationStatus() {
	return activationStatus;
    }

    public void setActivationStatus(ActivationStatusEnum activationStatus) {
	this.activationStatus = activationStatus;
    }

    public OrderItemType validFor(TimePeriodType validFor) {
	this.validFor = validFor;
	return this;
    }

    /**
     * The validity period requested or recorded for the product. If not included,
     * the current date is used as starting date and no ending date is defined. For
     * subscription creation (adding a product to an account), the ending date
     * indicates the termination date defined for the product
     * 
     * @return validFor
     **/

    @Valid

    public TimePeriodType getValidFor() {
	return validFor;
    }

    public void setValidFor(TimePeriodType validFor) {
	this.validFor = validFor;
    }

    public OrderItemType isAutoRenew(Boolean isAutoRenew) {
	this.isAutoRenew = isAutoRenew;
	return this;
    }

    /**
     * Indicates if the product that is associated to the account is renewed
     * periodically (acting as a subscription). When this is set to TRUE the
     * products will be removed automatically (or set to inactive state) when
     * renewal period expires
     * 
     * @return isAutoRenew
     **/

    public Boolean isIsAutoRenew() {
	return isAutoRenew;
    }

    public void setIsAutoRenew(Boolean isAutoRenew) {
	this.isAutoRenew = isAutoRenew;
    }

    public OrderItemType recurringPeriod(String recurringPeriod) {
	this.recurringPeriod = recurringPeriod;
	return this;
    }

    /**
     * Only when isAutoRenew is true. For auto renewable products indicates the
     * periodicity for the renewal operation (monthly, weekly, etc.)
     * 
     * @return recurringPeriod
     **/

    public String getRecurringPeriod() {
	return recurringPeriod;
    }

    public void setRecurringPeriod(String recurringPeriod) {
	this.recurringPeriod = recurringPeriod;
    }

    public OrderItemType nrOfPeriods(Integer nrOfPeriods) {
	this.nrOfPeriods = nrOfPeriods;
	return this;
    }

    /**
     * Only when isAutoRenew is true. For auto renewable products indicates the
     * number of occurrences of the period the renewal operation must be executed.
     * If not included then no limit is set.
     * 
     * @return nrOfPeriods
     **/

    public Integer getNrOfPeriods() {
	return nrOfPeriods;
    }

    public void setNrOfPeriods(Integer nrOfPeriods) {
	this.nrOfPeriods = nrOfPeriods;
    }

    public OrderItemType additionalData(List<KeyValueType> additionalData) {
	this.additionalData = additionalData;
	return this;
    }

    public OrderItemType addAdditionalDataItem(KeyValueType additionalDataItem) {
	if (this.additionalData == null) {
	    this.additionalData = new ArrayList<KeyValueType>();
	}
	this.additionalData.add(additionalDataItem);
	return this;
    }

    /**
     * Any further information needed by the server to fill the entity definition.
     * It is recommended not to use this parameter and request new information
     * elements to be added in the structure definition. Next releases of the T-Open
     * API will not include support for this additionalData parameter because it has
     * been detected that the extensibility function is not helping the stability of
     * the standard definition of APIs
     * 
     * @return additionalData
     **/

    @Valid

    public List<KeyValueType> getAdditionalData() {
	return additionalData;
    }

    public void setAdditionalData(List<KeyValueType> additionalData) {
	this.additionalData = additionalData;
    }

    @Override
    public boolean equals(java.lang.Object o) {
	if (this == o) {
	    return true;
	}
	if (o == null || getClass() != o.getClass()) {
	    return false;
	}
	OrderItemType orderItemType = (OrderItemType) o;
	return Objects.equals(this.id, orderItemType.id) && Objects.equals(this.description, orderItemType.description)
		&& Objects.equals(this.action, orderItemType.action) && Objects.equals(this.parent, orderItemType.parent)
		&& Objects.equals(this.productOffering, orderItemType.productOffering)
		&& Objects.equals(this.product, orderItemType.product) && Objects.equals(this.quantity, orderItemType.quantity)
		&& Objects.equals(this.minQuantity, orderItemType.minQuantity)
		&& Objects.equals(this.maxQuantity, orderItemType.maxQuantity)
		&& Objects.equals(this.orderItemPrice, orderItemType.orderItemPrice)
		&& Objects.equals(this.billingAccount, orderItemType.billingAccount)
		&& Objects.equals(this.paymentMethod, orderItemType.paymentMethod) && Objects.equals(this.status, orderItemType.status)
		&& Objects.equals(this.statusReason, orderItemType.statusReason)
		&& Objects.equals(this.activationStatus, orderItemType.activationStatus)
		&& Objects.equals(this.validFor, orderItemType.validFor) && Objects.equals(this.isAutoRenew, orderItemType.isAutoRenew)
		&& Objects.equals(this.recurringPeriod, orderItemType.recurringPeriod)
		&& Objects.equals(this.nrOfPeriods, orderItemType.nrOfPeriods)
		&& Objects.equals(this.additionalData, orderItemType.additionalData);
    }

    @Override
    public int hashCode() {
	return Objects.hash(id, description, action, parent, productOffering, product, quantity, minQuantity, maxQuantity, orderItemPrice,
		billingAccount, paymentMethod, status, statusReason, activationStatus, validFor, isAutoRenew, recurringPeriod, nrOfPeriods,
		additionalData);
    }

    @Override
    public String toString() {
	StringBuilder sb = new StringBuilder();
	sb.append("class OrderItemType {\n");

	sb.append("    id: ").append(toIndentedString(id)).append("\n");
	sb.append("    description: ").append(toIndentedString(description)).append("\n");
	sb.append("    action: ").append(toIndentedString(action)).append("\n");
	sb.append("    parent: ").append(toIndentedString(parent)).append("\n");
	sb.append("    productOffering: ").append(toIndentedString(productOffering)).append("\n");
	sb.append("    product: ").append(toIndentedString(product)).append("\n");
	sb.append("    quantity: ").append(toIndentedString(quantity)).append("\n");
	sb.append("    minQuantity: ").append(toIndentedString(minQuantity)).append("\n");
	sb.append("    maxQuantity: ").append(toIndentedString(maxQuantity)).append("\n");
	sb.append("    orderItemPrice: ").append(toIndentedString(orderItemPrice)).append("\n");
	sb.append("    billingAccount: ").append(toIndentedString(billingAccount)).append("\n");
	sb.append("    paymentMethod: ").append(toIndentedString(paymentMethod)).append("\n");
	sb.append("    status: ").append(toIndentedString(status)).append("\n");
	sb.append("    statusReason: ").append(toIndentedString(statusReason)).append("\n");
	sb.append("    activationStatus: ").append(toIndentedString(activationStatus)).append("\n");
	sb.append("    validFor: ").append(toIndentedString(validFor)).append("\n");
	sb.append("    isAutoRenew: ").append(toIndentedString(isAutoRenew)).append("\n");
	sb.append("    recurringPeriod: ").append(toIndentedString(recurringPeriod)).append("\n");
	sb.append("    nrOfPeriods: ").append(toIndentedString(nrOfPeriods)).append("\n");
	sb.append("    additionalData: ").append(toIndentedString(additionalData)).append("\n");
	sb.append("}");
	return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(java.lang.Object o) {
	if (o == null) {
	    return "null";
	}
	return o.toString().replace("\n", "\n    ");
    }
}
