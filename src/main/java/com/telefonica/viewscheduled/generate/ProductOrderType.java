package com.telefonica.viewscheduled.generate;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * ProductOrderType
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-19T16:24:38.761Z")

public class ProductOrderType {
    @JsonProperty("id")
    private String id = null;

    @JsonProperty("href")
    private String href = null;

    @JsonProperty("correlationId")
    private String correlationId = null;

    @JsonProperty("description")
    private String description = null;

    @JsonProperty("productOrderType")
    private String productOrderType = null;

    @JsonProperty("productOrderClass")
    @Valid
    private List<CategoryTreeType> productOrderClass = null;

    @JsonProperty("orderActionId")
    private String orderActionId = null;

    @JsonProperty("customerId")
    private String customerId = null;

    @JsonProperty("priority")
    private Integer priority = null;

    @JsonProperty("requestedPriority")
    private Integer requestedPriority = null;

    @JsonProperty("relatedParty")
    @Valid
    private List<RelatedPartyRefType> relatedParty = new ArrayList<RelatedPartyRefType>();

    @JsonProperty("relatedObject")
    @Valid
    private List<RelatedObjectType> relatedObject = null;

    @JsonProperty("channel")
    @Valid
    private List<ChannelRefType> channel = null;

    @JsonProperty("requestedStartDate")
    private OffsetDateTime requestedStartDate = null;

    @JsonProperty("requestedCompletionDate")
    private OffsetDateTime requestedCompletionDate = null;

    @JsonProperty("orderDate")
    private OffsetDateTime orderDate = null;

    @JsonProperty("expectedCompletionDate")
    private OffsetDateTime expectedCompletionDate = null;

    @JsonProperty("completionDate")
    private OffsetDateTime completionDate = null;

    @JsonProperty("note")
    @Valid
    private List<NoteInfoType> note = null;

    /**
     * Tracks the lifecycle status of the product order in the internal ordering
     * state machine of the processing system
     */
    public enum StatusEnum {
	NEW("new"),

	SUBMITTED("submitted"),

	REJECTED("rejected"),

	ACKNOWLEDGED("acknowledged"),

	IN_PROGRESS("in progress"),

	HELD("held"),

	CANCELLED("cancelled"),

	PARTIAL("partial"),

	PENDING("pending"),

	FAILED("failed"),

	COMPLETED("completed");

	private String value;

	StatusEnum(String value) {
	    this.value = value;
	}

	@Override
	@JsonValue
	public String toString() {
	    return String.valueOf(value);
	}

	@JsonCreator
	public static StatusEnum fromValue(String text) {
	    for (StatusEnum b : StatusEnum.values()) {
		if (String.valueOf(b.value).equals(text)) {
		    return b;
		}
	    }
	    return null;
	}
    }

    @JsonProperty("status")
    private StatusEnum status = null;

    @JsonProperty("subStatus")
    private String subStatus = null;

    @JsonProperty("statusReason")
    private String statusReason = null;

    @JsonProperty("orderItem")
    @Valid
    private List<OrderItemType> orderItem = new ArrayList<OrderItemType>();

    @JsonProperty("deploymentWorks")
    @Valid
    private List<DeploymentWorkType> deploymentWorks = null;

    @JsonProperty("additionalData")
    @Valid
    private List<KeyValueType> additionalData = null;

    public ProductOrderType id(String id) {
	this.id = id;
	return this;
    }

    /**
     * Unique Identifier within the server for the order reported
     * 
     * @return id
     **/

    @NotNull

    public String getId() {
	return id;
    }

    public void setId(String id) {
	this.id = id;
    }

    public ProductOrderType href(String href) {
	this.href = href;
	return this;
    }

    /**
     * A resource URI pointing to the resource in the OB that stores the order
     * detailed information
     * 
     * @return href
     **/

    @NotNull

    public String getHref() {
	return href;
    }

    public void setHref(String href) {
	this.href = href;
    }

    public ProductOrderType correlationId(String correlationId) {
	this.correlationId = correlationId;
	return this;
    }

    /**
     * Unique identifier for the order created within the client, used to
     * synchronize and map internal identifiers between server and client. Notice
     * that in the TMForum API version 16.5 this parameter is named externalId
     * 
     * @return correlationId
     **/

    public String getCorrelationId() {
	return correlationId;
    }

    public void setCorrelationId(String correlationId) {
	this.correlationId = correlationId;
    }

    public ProductOrderType description(String description) {
	this.description = description;
	return this;
    }

    /**
     * Some text providing a user-friendly detailed description of the product order
     * 
     * @return description
     **/

    public String getDescription() {
	return description;
    }

    public void setDescription(String description) {
	this.description = description;
    }

    public ProductOrderType productOrderType(String productOrderType) {
	this.productOrderType = productOrderType;
	return this;
    }

    /**
     * Indication of the type of order registered. Supported values are
     * implementation and application specific. Used to categorize the order from a
     * business perspective that can be useful for the OM system (e.g.: enterprise,
     * residential, ...). Notice that in the TMForum API version 16.5 this parameter
     * is named category
     * 
     * @return productOrderType
     **/

    public String getProductOrderType() {
	return productOrderType;
    }

    public void setProductOrderType(String productOrderType) {
	this.productOrderType = productOrderType;
    }

    public ProductOrderType productOrderClass(List<CategoryTreeType> productOrderClass) {
	this.productOrderClass = productOrderClass;
	return this;
    }

    public ProductOrderType addProductOrderClassItem(CategoryTreeType productOrderClassItem) {
	if (this.productOrderClass == null) {
	    this.productOrderClass = new ArrayList<CategoryTreeType>();
	}
	this.productOrderClass.add(productOrderClassItem);
	return this;
    }

    /**
     * A class defining the categorization of the product order. A product order may
     * belong to more than one class. A standard list of classes
     * (e.g.:Video,Audio,Adult, etc). Each implementation may define its own classes
     * 
     * @return productOrderClass
     **/

    @Valid

    public List<CategoryTreeType> getProductOrderClass() {
	return productOrderClass;
    }

    public void setProductOrderClass(List<CategoryTreeType> productOrderClass) {
	this.productOrderClass = productOrderClass;
    }

    public ProductOrderType orderActionId(String orderActionId) {
	this.orderActionId = orderActionId;
	return this;
    }

    /**
     * Typically orders can be decomposed in order actions
     * 
     * @return orderActionId
     **/

    public String getOrderActionId() {
	return orderActionId;
    }

    public void setOrderActionId(String orderActionId) {
	this.orderActionId = orderActionId;
    }

    public ProductOrderType customerId(String customerId) {
	this.customerId = customerId;
	return this;
    }

    /**
     * Unique identifier of the customer
     * 
     * @return customerId
     **/

    public String getCustomerId() {
	return customerId;
    }

    public void setCustomerId(String customerId) {
	this.customerId = customerId;
    }

    public ProductOrderType priority(Integer priority) {
	this.priority = priority;
	return this;
    }

    /**
     * Indication of the priority considered by the originator of the order. An
     * example of possible values are: 3 (low), 2 (medium), 1 (high), 0 (critical)
     * 
     * @return priority
     **/

    public Integer getPriority() {
	return priority;
    }

    public void setPriority(Integer priority) {
	this.priority = priority;
    }

    public ProductOrderType requestedPriority(Integer requestedPriority) {
	this.requestedPriority = requestedPriority;
	return this;
    }

    /**
     * Value of priority requested when creating theorder
     * 
     * @return requestedPriority
     **/

    public Integer getRequestedPriority() {
	return requestedPriority;
    }

    public void setRequestedPriority(Integer requestedPriority) {
	this.requestedPriority = requestedPriority;
    }

    public ProductOrderType relatedParty(List<RelatedPartyRefType> relatedParty) {
	this.relatedParty = relatedParty;
	return this;
    }

    public ProductOrderType addRelatedPartyItem(RelatedPartyRefType relatedPartyItem) {
	this.relatedParty.add(relatedPartyItem);
	return this;
    }

    /**
     * List of individuals (e.g.: support agent, system impacted, reviewer, u
     * associated to the order
     * 
     * @return relatedParty
     **/

    @NotNull

    @Valid

    public List<RelatedPartyRefType> getRelatedParty() {
	return relatedParty;
    }

    public void setRelatedParty(List<RelatedPartyRefType> relatedParty) {
	this.relatedParty = relatedParty;
    }

    public ProductOrderType relatedObject(List<RelatedObjectType> relatedObject) {
	this.relatedObject = relatedObject;
	return this;
    }

    public ProductOrderType addRelatedObjectItem(RelatedObjectType relatedObjectItem) {
	if (this.relatedObject == null) {
	    this.relatedObject = new ArrayList<RelatedObjectType>();
	}
	this.relatedObject.add(relatedObjectItem);
	return this;
    }

    /**
     * List of Objects or resources associated to an order (e.g.: framework
     * agreement, opportunity, u
     * 
     * @return relatedObject
     **/

    @Valid

    public List<RelatedObjectType> getRelatedObject() {
	return relatedObject;
    }

    public void setRelatedObject(List<RelatedObjectType> relatedObject) {
	this.relatedObject = relatedObject;
    }

    public ProductOrderType channel(List<ChannelRefType> channel) {
	this.channel = channel;
	return this;
    }

    public ProductOrderType addChannelItem(ChannelRefType channelItem) {
	if (this.channel == null) {
	    this.channel = new ArrayList<ChannelRefType>();
	}
	this.channel.add(channelItem);
	return this;
    }

    /**
     * Defines the channel that is used to trigger the order. This element is
     * defined as an array in order to allow the option to associate an order to
     * multiple channels
     * 
     * @return channel
     **/

    @Valid

    public List<ChannelRefType> getChannel() {
	return channel;
    }

    public void setChannel(List<ChannelRefType> channel) {
	this.channel = channel;
    }

    public ProductOrderType requestedStartDate(OffsetDateTime requestedStartDate) {
	this.requestedStartDate = requestedStartDate;
	return this;
    }

    /**
     * Order start date wished by the requestor (e.g.: date when delivery process
     * should begin). This could be required in order to manage SLA compliancy
     * 
     * @return requestedStartDate
     **/

    @Valid

    public OffsetDateTime getRequestedStartDate() {
	return requestedStartDate;
    }

    public void setRequestedStartDate(OffsetDateTime requestedStartDate) {
	this.requestedStartDate = requestedStartDate;
    }

    public ProductOrderType requestedCompletionDate(OffsetDateTime requestedCompletionDate) {
	this.requestedCompletionDate = requestedCompletionDate;
	return this;
    }

    /**
     * Requested delivery date from the requestor perspective (e.g.: date when
     * delivery of all the items within an order should be complete). This could be
     * required in order to manage SLA compliancy
     * 
     * @return requestedCompletionDate
     **/

    @Valid

    public OffsetDateTime getRequestedCompletionDate() {
	return requestedCompletionDate;
    }

    public void setRequestedCompletionDate(OffsetDateTime requestedCompletionDate) {
	this.requestedCompletionDate = requestedCompletionDate;
    }

    public ProductOrderType orderDate(OffsetDateTime orderDate) {
	this.orderDate = orderDate;
	return this;
    }

    /**
     * Date when the order was created in the server
     * 
     * @return orderDate
     **/

    @NotNull

    @Valid

    public OffsetDateTime getOrderDate() {
	return orderDate;
    }

    public void setOrderDate(OffsetDateTime orderDate) {
	this.orderDate = orderDate;
    }

    public ProductOrderType expectedCompletionDate(OffsetDateTime expectedCompletionDate) {
	this.expectedCompletionDate = expectedCompletionDate;
	return this;
    }

    /**
     * Expected delivery date from the provider perspective (e.g.: date when
     * delivery of all the items within an order are planned to be complete)
     * 
     * @return expectedCompletionDate
     **/

    @Valid

    public OffsetDateTime getExpectedCompletionDate() {
	return expectedCompletionDate;
    }

    public void setExpectedCompletionDate(OffsetDateTime expectedCompletionDate) {
	this.expectedCompletionDate = expectedCompletionDate;
    }

    public ProductOrderType completionDate(OffsetDateTime completionDate) {
	this.completionDate = completionDate;
	return this;
    }

    /**
     * Actual date when delivery of all the items within an order was complete. This
     * could be required in order to manage SLA compliancy
     * 
     * @return completionDate
     **/

    @Valid

    public OffsetDateTime getCompletionDate() {
	return completionDate;
    }

    public void setCompletionDate(OffsetDateTime completionDate) {
	this.completionDate = completionDate;
    }

    public ProductOrderType note(List<NoteInfoType> note) {
	this.note = note;
	return this;
    }

    public ProductOrderType addNoteItem(NoteInfoType noteItem) {
	if (this.note == null) {
	    this.note = new ArrayList<NoteInfoType>();
	}
	this.note.add(noteItem);
	return this;
    }

    /**
     * List of notes added as part of the order
     * 
     * @return note
     **/

    @Valid

    public List<NoteInfoType> getNote() {
	return note;
    }

    public void setNote(List<NoteInfoType> note) {
	this.note = note;
    }

    public ProductOrderType status(StatusEnum status) {
	this.status = status;
	return this;
    }

    /**
     * Tracks the lifecycle status of the product order in the internal ordering
     * state machine of the processing system
     * 
     * @return status
     **/

    @NotNull

    public StatusEnum getStatus() {
	return status;
    }

    public void setStatus(StatusEnum status) {
	this.status = status;
    }

    public ProductOrderType subStatus(String subStatus) {
	this.subStatus = subStatus;
	return this;
    }

    /**
     * Substatus in order to define a second status level
     * 
     * @return subStatus
     **/

    public String getSubStatus() {
	return subStatus;
    }

    public void setSubStatus(String subStatus) {
	this.subStatus = subStatus;
    }

    public ProductOrderType statusReason(String statusReason) {
	this.statusReason = statusReason;
	return this;
    }

    /**
     * Reasoning for the status/substatus, for instance reasoning for an order
     * process failed
     * 
     * @return statusReason
     **/

    public String getStatusReason() {
	return statusReason;
    }

    public void setStatusReason(String statusReason) {
	this.statusReason = statusReason;
    }

    public ProductOrderType orderItem(List<OrderItemType> orderItem) {
	this.orderItem = orderItem;
	return this;
    }

    public ProductOrderType addOrderItemItem(OrderItemType orderItemItem) {
	this.orderItem.add(orderItemItem);
	return this;
    }

    /**
     * List of individual items included in the order
     * 
     * @return orderItem
     **/

    @NotNull

    @Valid
    @Size(min = 1)
    public List<OrderItemType> getOrderItem() {
	return orderItem;
    }

    public void setOrderItem(List<OrderItemType> orderItem) {
	this.orderItem = orderItem;
    }

    public ProductOrderType deploymentWorks(List<DeploymentWorkType> deploymentWorks) {
	this.deploymentWorks = deploymentWorks;
	return this;
    }

    public ProductOrderType addDeploymentWorksItem(DeploymentWorkType deploymentWorksItem) {
	if (this.deploymentWorks == null) {
	    this.deploymentWorks = new ArrayList<DeploymentWorkType>();
	}
	this.deploymentWorks.add(deploymentWorksItem);
	return this;
    }

    /**
     * When an order item implies some sort of work such as cable deployments,
     * towers, etc. Typically used in B2B, since in B2C end customers are abstracted
     * from these tasks
     * 
     * @return deploymentWorks
     **/

    @Valid

    public List<DeploymentWorkType> getDeploymentWorks() {
	return deploymentWorks;
    }

    public void setDeploymentWorks(List<DeploymentWorkType> deploymentWorks) {
	this.deploymentWorks = deploymentWorks;
    }

    public ProductOrderType additionalData(List<KeyValueType> additionalData) {
	this.additionalData = additionalData;
	return this;
    }

    public ProductOrderType addAdditionalDataItem(KeyValueType additionalDataItem) {
	if (this.additionalData == null) {
	    this.additionalData = new ArrayList<KeyValueType>();
	}
	this.additionalData.add(additionalDataItem);
	return this;
    }

    /**
     * Any further information needed by the server to fill the entity definition.
     * It is recommended not to use this parameter and request new information
     * elements to be added in the structure definition. Next releases of the T-Open
     * API will not include support for this additionalData parameter because it has
     * been detected that the extensibility function is not helping the stability of
     * the standard definition of APIs
     * 
     * @return additionalData
     **/

    @Valid

    public List<KeyValueType> getAdditionalData() {
	return additionalData;
    }

    public void setAdditionalData(List<KeyValueType> additionalData) {
	this.additionalData = additionalData;
    }

    @Override
    public boolean equals(java.lang.Object o) {
	if (this == o) {
	    return true;
	}
	if (o == null || getClass() != o.getClass()) {
	    return false;
	}
	ProductOrderType productOrderType = (ProductOrderType) o;
	return Objects.equals(this.id, productOrderType.id) && Objects.equals(this.href, productOrderType.href)
		&& Objects.equals(this.correlationId, productOrderType.correlationId)
		&& Objects.equals(this.description, productOrderType.description)
		&& Objects.equals(this.productOrderType, productOrderType.productOrderType)
		&& Objects.equals(this.productOrderClass, productOrderType.productOrderClass)
		&& Objects.equals(this.orderActionId, productOrderType.orderActionId)
		&& Objects.equals(this.customerId, productOrderType.customerId) && Objects.equals(this.priority, productOrderType.priority)
		&& Objects.equals(this.requestedPriority, productOrderType.requestedPriority)
		&& Objects.equals(this.relatedParty, productOrderType.relatedParty)
		&& Objects.equals(this.relatedObject, productOrderType.relatedObject)
		&& Objects.equals(this.channel, productOrderType.channel)
		&& Objects.equals(this.requestedStartDate, productOrderType.requestedStartDate)
		&& Objects.equals(this.requestedCompletionDate, productOrderType.requestedCompletionDate)
		&& Objects.equals(this.orderDate, productOrderType.orderDate)
		&& Objects.equals(this.expectedCompletionDate, productOrderType.expectedCompletionDate)
		&& Objects.equals(this.completionDate, productOrderType.completionDate) && Objects.equals(this.note, productOrderType.note)
		&& Objects.equals(this.status, productOrderType.status) && Objects.equals(this.subStatus, productOrderType.subStatus)
		&& Objects.equals(this.statusReason, productOrderType.statusReason)
		&& Objects.equals(this.orderItem, productOrderType.orderItem)
		&& Objects.equals(this.deploymentWorks, productOrderType.deploymentWorks)
		&& Objects.equals(this.additionalData, productOrderType.additionalData);
    }

    @Override
    public int hashCode() {
	return Objects.hash(id, href, correlationId, description, productOrderType, productOrderClass, orderActionId, customerId, priority,
		requestedPriority, relatedParty, relatedObject, channel, requestedStartDate, requestedCompletionDate, orderDate,
		expectedCompletionDate, completionDate, note, status, subStatus, statusReason, orderItem, deploymentWorks, additionalData);
    }

    @Override
    public String toString() {
	StringBuilder sb = new StringBuilder();
	sb.append("class ProductOrderType {\n");

	sb.append("    id: ").append(toIndentedString(id)).append("\n");
	sb.append("    href: ").append(toIndentedString(href)).append("\n");
	sb.append("    correlationId: ").append(toIndentedString(correlationId)).append("\n");
	sb.append("    description: ").append(toIndentedString(description)).append("\n");
	sb.append("    productOrderType: ").append(toIndentedString(productOrderType)).append("\n");
	sb.append("    productOrderClass: ").append(toIndentedString(productOrderClass)).append("\n");
	sb.append("    orderActionId: ").append(toIndentedString(orderActionId)).append("\n");
	sb.append("    customerId: ").append(toIndentedString(customerId)).append("\n");
	sb.append("    priority: ").append(toIndentedString(priority)).append("\n");
	sb.append("    requestedPriority: ").append(toIndentedString(requestedPriority)).append("\n");
	sb.append("    relatedParty: ").append(toIndentedString(relatedParty)).append("\n");
	sb.append("    relatedObject: ").append(toIndentedString(relatedObject)).append("\n");
	sb.append("    channel: ").append(toIndentedString(channel)).append("\n");
	sb.append("    requestedStartDate: ").append(toIndentedString(requestedStartDate)).append("\n");
	sb.append("    requestedCompletionDate: ").append(toIndentedString(requestedCompletionDate)).append("\n");
	sb.append("    orderDate: ").append(toIndentedString(orderDate)).append("\n");
	sb.append("    expectedCompletionDate: ").append(toIndentedString(expectedCompletionDate)).append("\n");
	sb.append("    completionDate: ").append(toIndentedString(completionDate)).append("\n");
	sb.append("    note: ").append(toIndentedString(note)).append("\n");
	sb.append("    status: ").append(toIndentedString(status)).append("\n");
	sb.append("    subStatus: ").append(toIndentedString(subStatus)).append("\n");
	sb.append("    statusReason: ").append(toIndentedString(statusReason)).append("\n");
	sb.append("    orderItem: ").append(toIndentedString(orderItem)).append("\n");
	sb.append("    deploymentWorks: ").append(toIndentedString(deploymentWorks)).append("\n");
	sb.append("    additionalData: ").append(toIndentedString(additionalData)).append("\n");
	sb.append("}");
	return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(java.lang.Object o) {
	if (o == null) {
	    return "null";
	}
	return o.toString().replace("\n", "\n    ");
    }
}
