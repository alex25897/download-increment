package com.telefonica.viewscheduled.generate;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * NoteInfoType
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-19T16:24:38.761Z")

public class NoteInfoType {
    @JsonProperty("date")
    private OffsetDateTime date = null;

    @JsonProperty("author")
    private String author = null;

    @JsonProperty("text")
    private String text = null;

    @JsonProperty("additionalData")
    @Valid
    private List<KeyValueType> additionalData = null;

    public NoteInfoType date(OffsetDateTime date) {
	this.date = date;
	return this;
    }

    /**
     * Date when the note was created to the order
     * 
     * @return date
     **/

    @NotNull

    @Valid

    public OffsetDateTime getDate() {
	return date;
    }

    public void setDate(OffsetDateTime date) {
	this.date = date;
    }

    public NoteInfoType author(String author) {
	this.author = author;
	return this;
    }

    /**
     * Identification of the originator of the note. Meaning of this is
     * implementation and application specific, it could be, for instance, the login
     * name of a persona that could access to read/modify the order details via a
     * web portal
     * 
     * @return author
     **/

    public String getAuthor() {
	return author;
    }

    public void setAuthor(String author) {
	this.author = author;
    }

    public NoteInfoType text(String text) {
	this.text = text;
	return this;
    }

    /**
     * Contents of the note (comment) to be associated to the order
     * 
     * @return text
     **/

    @NotNull

    public String getText() {
	return text;
    }

    public void setText(String text) {
	this.text = text;
    }

    public NoteInfoType additionalData(List<KeyValueType> additionalData) {
	this.additionalData = additionalData;
	return this;
    }

    public NoteInfoType addAdditionalDataItem(KeyValueType additionalDataItem) {
	if (this.additionalData == null) {
	    this.additionalData = new ArrayList<KeyValueType>();
	}
	this.additionalData.add(additionalDataItem);
	return this;
    }

    /**
     * Any further information needed by the server to fill the entity definition.
     * It is recommended not to use this parameter and request new information
     * elements to be added in the structure definition. Next releases of the T-Open
     * API will not include support for this additionalData parameter because it has
     * been detected that the extensibility function is not helping the stability of
     * the standard definition of APIs
     * 
     * @return additionalData
     **/

    @Valid

    public List<KeyValueType> getAdditionalData() {
	return additionalData;
    }

    public void setAdditionalData(List<KeyValueType> additionalData) {
	this.additionalData = additionalData;
    }

    @Override
    public boolean equals(java.lang.Object o) {
	if (this == o) {
	    return true;
	}
	if (o == null || getClass() != o.getClass()) {
	    return false;
	}
	NoteInfoType noteInfoType = (NoteInfoType) o;
	return Objects.equals(this.date, noteInfoType.date) && Objects.equals(this.author, noteInfoType.author)
		&& Objects.equals(this.text, noteInfoType.text) && Objects.equals(this.additionalData, noteInfoType.additionalData);
    }

    @Override
    public int hashCode() {
	return Objects.hash(date, author, text, additionalData);
    }

    @Override
    public String toString() {
	StringBuilder sb = new StringBuilder();
	sb.append("class NoteInfoType {\n");

	sb.append("    date: ").append(toIndentedString(date)).append("\n");
	sb.append("    author: ").append(toIndentedString(author)).append("\n");
	sb.append("    text: ").append(toIndentedString(text)).append("\n");
	sb.append("    additionalData: ").append(toIndentedString(additionalData)).append("\n");
	sb.append("}");
	return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(java.lang.Object o) {
	if (o == null) {
	    return "null";
	}
	return o.toString().replace("\n", "\n    ");
    }
}
