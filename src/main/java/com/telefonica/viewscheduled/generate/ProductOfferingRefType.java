package com.telefonica.viewscheduled.generate;

import java.util.Objects;

import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * ProductOfferingRefType
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-19T16:24:38.761Z")

public class ProductOfferingRefType {
    @JsonProperty("id")
    private String id = null;

    @JsonProperty("href")
    private String href = null;

    @JsonProperty("name")
    private String name = null;

    @JsonProperty("@referredType")
    private String referredType = null;

    public ProductOfferingRefType id(String id) {
	this.id = id;
	return this;
    }

    /**
     * Unique identifier of the product offering
     * 
     * @return id
     **/

    @NotNull

    public String getId() {
	return id;
    }

    public void setId(String id) {
	this.id = id;
    }

    public ProductOfferingRefType href(String href) {
	this.href = href;
	return this;
    }

    /**
     * A resource URI pointing to the resource in the OB that stores the entity
     * detailed information. It can be queried using the Product Catalog Management
     * API
     * 
     * @return href
     **/

    public String getHref() {
	return href;
    }

    public void setHref(String href) {
	this.href = href;
    }

    public ProductOfferingRefType name(String name) {
	this.name = name;
	return this;
    }

    /**
     * Name of the product offering
     * 
     * @return name
     **/

    public String getName() {
	return name;
    }

    public void setName(String name) {
	this.name = name;
    }

    public ProductOfferingRefType referredType(String referredType) {
	this.referredType = referredType;
	return this;
    }

    /**
     * Identifier of the type of offering that is being referred
     * 
     * @return referredType
     **/

    public String getReferredType() {
	return referredType;
    }

    public void setReferredType(String referredType) {
	this.referredType = referredType;
    }

    @Override
    public boolean equals(java.lang.Object o) {
	if (this == o) {
	    return true;
	}
	if (o == null || getClass() != o.getClass()) {
	    return false;
	}
	ProductOfferingRefType productOfferingRefType = (ProductOfferingRefType) o;
	return Objects.equals(this.id, productOfferingRefType.id) && Objects.equals(this.href, productOfferingRefType.href)
		&& Objects.equals(this.name, productOfferingRefType.name)
		&& Objects.equals(this.referredType, productOfferingRefType.referredType);
    }

    @Override
    public int hashCode() {
	return Objects.hash(id, href, name, referredType);
    }

    @Override
    public String toString() {
	StringBuilder sb = new StringBuilder();
	sb.append("class ProductOfferingRefType {\n");

	sb.append("    id: ").append(toIndentedString(id)).append("\n");
	sb.append("    href: ").append(toIndentedString(href)).append("\n");
	sb.append("    name: ").append(toIndentedString(name)).append("\n");
	sb.append("    referredType: ").append(toIndentedString(referredType)).append("\n");
	sb.append("}");
	return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(java.lang.Object o) {
	if (o == null) {
	    return "null";
	}
	return o.toString().replace("\n", "\n    ");
    }
}
