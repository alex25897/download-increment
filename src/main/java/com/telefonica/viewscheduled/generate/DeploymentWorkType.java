package com.telefonica.viewscheduled.generate;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.validation.Valid;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * DeploymentWorkType
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-19T16:24:38.761Z")

public class DeploymentWorkType {
    @JsonProperty("name")
    private String name = null;

    @JsonProperty("orderItems")
    @Valid
    private List<String> orderItems = null;

    @JsonProperty("costs")
    @Valid
    private List<DeploymentWorkCostType> costs = null;

    @JsonProperty("characteristics")
    @Valid
    private List<KeyValueType> characteristics = null;

    public DeploymentWorkType name(String name) {
	this.name = name;
	return this;
    }

    /**
     * Name of the work to be done
     * 
     * @return name
     **/

    public String getName() {
	return name;
    }

    public void setName(String name) {
	this.name = name;
    }

    public DeploymentWorkType orderItems(List<String> orderItems) {
	this.orderItems = orderItems;
	return this;
    }

    public DeploymentWorkType addOrderItemsItem(String orderItemsItem) {
	if (this.orderItems == null) {
	    this.orderItems = new ArrayList<String>();
	}
	this.orderItems.add(orderItemsItem);
	return this;
    }

    /**
     * Pointers to the order items this deployment work applies to
     * 
     * @return orderItems
     **/

    public List<String> getOrderItems() {
	return orderItems;
    }

    public void setOrderItems(List<String> orderItems) {
	this.orderItems = orderItems;
    }

    public DeploymentWorkType costs(List<DeploymentWorkCostType> costs) {
	this.costs = costs;
	return this;
    }

    public DeploymentWorkType addCostsItem(DeploymentWorkCostType costsItem) {
	if (this.costs == null) {
	    this.costs = new ArrayList<DeploymentWorkCostType>();
	}
	this.costs.add(costsItem);
	return this;
    }

    /**
     * Costs of the deployment work
     * 
     * @return costs
     **/

    @Valid

    public List<DeploymentWorkCostType> getCosts() {
	return costs;
    }

    public void setCosts(List<DeploymentWorkCostType> costs) {
	this.costs = costs;
    }

    public DeploymentWorkType characteristics(List<KeyValueType> characteristics) {
	this.characteristics = characteristics;
	return this;
    }

    public DeploymentWorkType addCharacteristicsItem(KeyValueType characteristicsItem) {
	if (this.characteristics == null) {
	    this.characteristics = new ArrayList<KeyValueType>();
	}
	this.characteristics.add(characteristicsItem);
	return this;
    }

    /**
     * Extra characteristics of the work
     * 
     * @return characteristics
     **/

    @Valid

    public List<KeyValueType> getCharacteristics() {
	return characteristics;
    }

    public void setCharacteristics(List<KeyValueType> characteristics) {
	this.characteristics = characteristics;
    }

    @Override
    public boolean equals(java.lang.Object o) {
	if (this == o) {
	    return true;
	}
	if (o == null || getClass() != o.getClass()) {
	    return false;
	}
	DeploymentWorkType deploymentWorkType = (DeploymentWorkType) o;
	return Objects.equals(this.name, deploymentWorkType.name) && Objects.equals(this.orderItems, deploymentWorkType.orderItems)
		&& Objects.equals(this.costs, deploymentWorkType.costs)
		&& Objects.equals(this.characteristics, deploymentWorkType.characteristics);
    }

    @Override
    public int hashCode() {
	return Objects.hash(name, orderItems, costs, characteristics);
    }

    @Override
    public String toString() {
	StringBuilder sb = new StringBuilder();
	sb.append("class DeploymentWorkType {\n");

	sb.append("    name: ").append(toIndentedString(name)).append("\n");
	sb.append("    orderItems: ").append(toIndentedString(orderItems)).append("\n");
	sb.append("    costs: ").append(toIndentedString(costs)).append("\n");
	sb.append("    characteristics: ").append(toIndentedString(characteristics)).append("\n");
	sb.append("}");
	return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(java.lang.Object o) {
	if (o == null) {
	    return "null";
	}
	return o.toString().replace("\n", "\n    ");
    }
}
