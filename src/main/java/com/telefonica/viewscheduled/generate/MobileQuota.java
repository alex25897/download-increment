package com.telefonica.viewscheduled.generate;

import java.util.Objects;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * MobileQuota
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-19T16:24:38.761Z")

public class MobileQuota extends ProductCharacteristicType {
    @JsonProperty("value")
    private MobileQuotaCharacteristicType value = null;

    @JsonProperty("@schemaLocation")
    private String schemaLocation = null;

    public MobileQuota value(MobileQuotaCharacteristicType value) {
	this.value = value;
	return this;
    }

    /**
     * Object including the characteristic information
     * 
     * @return value
     **/

    @NotNull

    @Valid

    public MobileQuotaCharacteristicType getValue() {
	return value;
    }

    public void setValue(MobileQuotaCharacteristicType value) {
	this.value = value;
    }

    public MobileQuota schemaLocation(String schemaLocation) {
	this.schemaLocation = schemaLocation;
	return this;
    }

    /**
     * This field provides a link to the schema describing the resource model for
     * the Object defining the characteristics for an internet product
     * 
     * @return schemaLocation
     **/

    public String getSchemaLocation() {
	return schemaLocation;
    }

    public void setSchemaLocation(String schemaLocation) {
	this.schemaLocation = schemaLocation;
    }

    @Override
    public boolean equals(java.lang.Object o) {
	if (this == o) {
	    return true;
	}
	if (o == null || getClass() != o.getClass()) {
	    return false;
	}
	MobileQuota mobileQuota = (MobileQuota) o;
	return Objects.equals(this.value, mobileQuota.value) && Objects.equals(this.schemaLocation, mobileQuota.schemaLocation)
		&& super.equals(o);
    }

    @Override
    public int hashCode() {
	return Objects.hash(value, schemaLocation, super.hashCode());
    }

    @Override
    public String toString() {
	StringBuilder sb = new StringBuilder();
	sb.append("class MobileQuota {\n");
	sb.append("    ").append(toIndentedString(super.toString())).append("\n");
	sb.append("    value: ").append(toIndentedString(value)).append("\n");
	sb.append("    schemaLocation: ").append(toIndentedString(schemaLocation)).append("\n");
	sb.append("}");
	return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(java.lang.Object o) {
	if (o == null) {
	    return "null";
	}
	return o.toString().replace("\n", "\n    ");
    }
}
